<?php

return [
    'adminEmail' => 'admin@example.com',
    'sphinx' => [
        'host' => 'localhost',
        'port' => 3312,
    ],
    'host' => 'kvt.loc/'
];
