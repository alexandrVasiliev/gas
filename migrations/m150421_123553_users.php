<?php

use yii\db\Schema;
use yii\db\Migration;

class m150421_123553_users extends Migration
{
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING . ' NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING . ' NOT NULL',

            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->createTable('auth_rule', [
            'name' => 'string',
            'data' => 'text',
            'created_at' => 'integer',
            'updated_at' => 'integer',
        ]);

        $this->addPrimaryKey('auth_rulePk', 'auth_rule', 'name');

        $this->createTable('auth_item', [
            'name' => 'string',
            'type' => 'integer',
            'description' => 'text',
            'rule_name' => 'string',
            'data' => 'text',
            'created_at' => 'integer',
            'updated_at' => 'integer',
        ]);

        $this->addPrimaryKey('auth_itemPk', 'auth_item', 'name');

        $this->addForeignKey('auth_itemToAuth_rule', 'auth_item', 'rule_name', 'auth_rule', 'name');

        $this->createTable('auth_item_child', [
            'parent' => 'string',
            'child' => 'string',
        ]);

        $this->addForeignKey('Auth_item_childParent', 'auth_item_child', 'parent' ,'auth_item', 'name');

        $this->addForeignKey('Auth_item_childChild', 'auth_item_child', 'child' ,'auth_item', 'name');

        $this->createTable('auth_assignment', [
            'item_name' => 'string',
            'user_id' => 'integer',
            'created_at' => 'integer',
        ]);

        $this->addPrimaryKey('auth_assignmentItem_name', 'auth_assignment', ['item_name', 'user_id']);

        $this->addForeignKey('auth_assignmentToAuth_item', 'auth_assignment', 'item_name', 'auth_item', 'name');

        $this->addForeignKey('auth_assignmentToUser', 'auth_assignment', 'user_id', 'user', 'id');

    }

    public function down()
    {
        $this->dropForeignKey('Auth_item_childParent', 'auth_item_child');
        $this->dropForeignKey('Auth_item_childChild', 'auth_item_child');
        $this->dropForeignKey('auth_itemToAuth_rule', 'auth_rule');
        $this->dropForeignKey('auth_assignmentToAuth_item', 'auth_assignment');
        $this->dropForeignKey('auth_assignmentToUser', 'auth_assignment');

        $this->dropTable('auth_rule');
        $this->dropTable('auth_item');
        $this->dropPrimaryKey('auth_itemPk', 'auth_item');
        $this->dropPrimaryKey('auth_rulePk', 'auth_rule');
        $this->dropTable('auth_item_child');
        $this->dropTable('{{%user}}');
        $this->dropTable('auth_assignment');
        $this->dropPrimaryKey('auth_assignmentItem_name', 'auth_assignment');
        $this->dropPrimaryKey('auth_assignmentItem_name', 'auth_assignment');

    }
}
