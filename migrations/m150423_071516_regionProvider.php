<?php

use yii\db\Schema;
use yii\db\Migration;

class m150423_071516_regionProvider extends Migration
{
    public function up()
    {
        $this->addColumn('regions', 'idProvider', 'integer');
        $this->addForeignKey('regionsProvider', 'regions', 'idProvider', 'providers', 'idProvider');
    }

    public function down()
    {
        $this->dropColumn('regions', 'idProvider');
        $this->dropForeignKey('regionsProvider', 'regions');
    }

}
