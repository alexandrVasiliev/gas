<?php

use yii\db\Schema;
use yii\db\Migration;

class m150409_130405_addConsumerName extends Migration
{
    public function up()
    {
        $this->addColumn('consumer', 'name', 'string');
    }

    public function down()
    {
        $this->dropColumn('consumer', 'name');
    }
}
