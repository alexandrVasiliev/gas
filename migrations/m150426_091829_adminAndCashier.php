<?php

use yii\db\Schema;
use yii\db\Migration;

class m150426_091829_adminAndCashier extends Migration
{
    public function up()
    {
        $this->batchInsert('auth_item', ['name', 'type', 'description'], [
            ['admin', 1, 'admin'],
            ['cashier', 1, 'cashier'],
        ]);

        $this->batchInsert('user', ['id', 'username', 'auth_key', 'password_hash', 'email', 'status', 'created_at', 'updated_at'], [
            [1, 'admin', '0GRQWYhHVOevjniC7x1IhhRjU-CrckHP', '$2y$13$2oC4QVOMYhueYfFbEdcVdO9e1B6mkGybvTcoGPmCjh77PAsspco1S', 'admin@gmail.com', 10, '1429620429', '1429620429'],
            [2, 'cashier', 'Mw81mIffd932hNhNm1zwX-K3glpL4IP-', '$2y$13$2oC4QVOMYhueYfFbEdcVdO9e1B6mkGybvTcoGPmCjh77PAsspco1S', 'cashier@gmail.com', 10, '1429620429', '1429620429'],

        ]);

        $this->batchInsert('auth_item_child', ['parent', 'child'], [
            ['admin', 'cashier'],
        ]);
        $this->batchInsert('auth_assignment', ['item_name', 'user_id'], [
            ['admin', '1'],
            ['cashier', '2'],
        ]);

    }

    public function down()
    {
        return false;
    }
}
