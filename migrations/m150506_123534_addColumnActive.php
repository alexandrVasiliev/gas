<?php

use yii\db\Schema;
use yii\db\Migration;

class m150506_123534_addColumnActive extends Migration
{
    public function up()
    {
        $this->addColumn('apartments', 'status', 'tinyint');
        $this->addColumn('houses', 'status', 'tinyint');
        $this->addColumn('streets', 'status', 'tinyint');
        $this->addColumn('cities', 'status', 'tinyint');
        $this->addColumn('areas', 'status', 'tinyint');
        $this->addColumn('regions', 'status', 'tinyint');
        $this->addColumn('consumer', 'status', 'tinyint');
        $this->addColumn('order', 'status', 'tinyint');
        $this->addColumn('providers', 'status', 'tinyint');
        $this->addColumn('clients', 'status', 'tinyint');
        $this->addColumn('benefit', 'status', 'tinyint');
    }

    public function down()
    {
        $this->dropColumn('apartments', 'status');
        $this->dropColumn('houses', 'status');
        $this->dropColumn('streets', 'status');
        $this->dropColumn('cities', 'status');
        $this->dropColumn('areas', 'status');
        $this->dropColumn('regions', 'status');
        $this->dropColumn('consumer', 'status');
        $this->dropColumn('order', 'status');
        $this->dropColumn('providers', 'status');
        $this->dropColumn('clients', 'status');
        $this->dropColumn('benefit', 'status');
    }

}
