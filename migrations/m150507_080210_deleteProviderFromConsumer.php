<?php

use yii\db\Schema;
use yii\db\Migration;

class m150507_080210_deleteProviderFromConsumer extends Migration
{
    public function up()
    {
        $this->dropForeignKey('consumerToProviders', 'consumer');
        $this->dropColumn('consumer', 'idProvider');
    }

    public function down()
    {
        $this->addForeignKey('consumerToProviders', 'consumer', 'idProvider', 'providers', 'idProvider'); // consumer-provider
        $this->addColumn('consumer', 'idProvider', 'integer');
    }

}
