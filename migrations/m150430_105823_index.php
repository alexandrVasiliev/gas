<?php

use yii\db\Schema;
use yii\db\Migration;

class m150430_105823_index extends Migration
{
    public function up()
    {
        $this->createIndex('index1', 'regions', 'region', true);
        $this->createIndex('index2', 'areas', 'area', true);
        $this->createIndex('index3', 'benefit', 'nameBenefit', true);
        $this->createIndex('index4', 'clients', 'personalAccount', true);
        $this->createIndex('index6', 'providers', 'name', true);
        $this->createIndex('index7', 'tariff', 'name', true);
    }

    public function down()
    {
        $this->dropIndex('index1', 'regions');
        $this->dropIndex('index2', 'areas');
        $this->dropIndex('index3', 'benefit');
        $this->dropIndex('index4', 'clients');
        $this->dropIndex('index6', 'providers');
        $this->dropIndex('index7', 'tariff');
    }

}
