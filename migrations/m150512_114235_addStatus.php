 <?php

use yii\db\Schema;
use yii\db\Migration;

class m150512_114235_addStatus extends Migration
{
    public function up()
    {
        $this->addColumn('persons', 'status', 'tinyint');
    }

    public function down()
    {
        $this->dropColumn('persons', 'status');
    }

}
