<?php

use yii\db\Schema;
use yii\db\Migration;

class m150325_131050_alter extends Migration
{
    public function up()
    {
        $this->addColumn('providers', 'name', 'string');
        $this->addColumn('order', 'name', 'string');
        $this->addColumn('tariff', 'name', 'string');
    }

    public function down()
    {
        $this->dropColumn('providers', 'name');
        $this->dropColumn('order', 'name');
        $this->dropColumn('tariff', 'name');

    }
}
