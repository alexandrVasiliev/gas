<?php

use yii\db\Schema;
use yii\db\Migration;

class m150324_145955_kvt2 extends Migration
{
    public function up()
    {
        ///////////////////////////////
        $this->createTable('benefit', [
                'idBenefit' => 'pk',
                'benefit' => 'float',
                'nameBenefit' => 'string',
                'limit' => 'integer',
            ]);
        ///////////////////////////////
        $this->createTable('clients', [
                'idClient' => 'pk',
                'idPerson' => 'integer',
                'idBenefit' => 'integer',
                'personalAccount' => 'string',
            ]);
        ///////////////////////////////
        $this->createTable('persons', [
                'idPerson' => 'pk',
                'firstname' => 'string',
                'middlename' => 'string',
                'lastname' => 'string',

            ]);
        ///////////////////////////////
        $this->createTable('regions', [
                'idRegion' => 'pk',
                'region' => 'string',
            ]);
        ///////////////////////////////
        $this->createTable('areas', [
                'idArea' => 'pk',
                'area' => 'string',
                'idRegion' => 'integer',
            ]);
        ///////////////////////////////
        $this->createTable('cities', [
                'idCity' => 'pk',
                'city' => 'string',
                'idArea' => 'integer',
            ]);
        ///////////////////////////////
        $this->createTable('streets', [
                'idStreet' => 'pk',
                'street' => 'string',
                'idCity' => 'integer',
            ]);
        ///////////////////////////////
        $this->createTable('houses', [
                'idHouse' => 'pk',
                'house' => 'string',
                'idStreet' => 'integer',
            ]);
        ///////////////////////////////
        $this->createTable('apartments', [
                'idApartment' => 'pk',
                'apartment' => 'string',
                'idHouse' => 'integer',
            ]);
        ///////////////////////////////
        $this->createTable('tariff', [
                'idTariff' => 'pk',
                'tariffAfter250' => 'float',
                'tariffUpTo250' => 'float',
            ]);
        ///////////////////////////////
        $this->createTable('providers', [
                'idProvider' => 'pk',
                'kc' => 'integer',
                'pr' => 'string',
                'mfo' => 'string',
                'edrpoy' => 'string',
            ]);
        ///////////////////////////////
        $this->createTable('debts', [
                'idDebt' => 'pk',
                'currentIndexAuto' => 'integer',
                'date' => 'date',
                'unpaidCurrentMonth' => 'integer',
                'idConsumer' => 'integer',
            ]);
        ///////////////////////////////
        $this->createTable('consumer', [
                'idConsumer' => 'pk',
                'idTariff' => 'integer',
                'idProvider' => 'integer',
                //'idOrder' => 'integer',
                'idApartment' => 'integer',
                'idClient' => 'integer',
            ]);
        ///////////////////////////////
        $this->createTable('order', [
                'idOrder' => 'pk',
                'date' => 'date',
                'tariffAfter250' => 'float',
                'tariffUpTo250' => 'float',
                'used' => 'integer',
                'paid' => 'float',
                'idCashier' => 'integer',
                'idConsumer' => 'integer',
        ]);
        ////////////////////////////foreign key
        $this->addForeignKey('clientToBenefit', 'clients', 'idBenefit', 'benefit', 'idBenefit'); // clients-benefit
        $this->addForeignKey('clientToPerson', 'clients', 'idPerson', 'persons', 'idPerson'); // clients-person
        $this->addForeignKey('consumerToClients', 'consumer', 'idClient', 'clients', 'idClient'); // consumer-clients
        $this->addForeignKey('consumerToApartments', 'consumer', 'idApartment', 'apartments', 'idApartment'); // consumer-apartment
        $this->addForeignKey('consumerToProviders', 'consumer', 'idProvider', 'providers', 'idProvider'); // consumer-provider
        $this->addForeignKey('consumerToOrder', 'order', 'idConsumer', 'consumer', 'idConsumer'); // consumer-order
        $this->addForeignKey('consumerToTariff', 'consumer', 'idTariff', 'tariff', 'idTariff'); // consumer-tariff
        $this->addForeignKey('apartmentToHouses', 'apartments', 'idHouse', 'houses', 'idHouse'); // apartment-houses
        $this->addForeignKey('housesToStreets', 'houses', 'idStreet', 'streets', 'idStreet'); // houses-streets
        $this->addForeignKey('streetsToCities', 'streets', 'idCity', 'cities', 'idCity'); // streets-cities
        $this->addForeignKey('citiesToAreas', 'cities', 'idArea', 'areas', 'idArea'); // cities-areas
        $this->addForeignKey('areasToRegions', 'areas', 'idRegion', 'regions', 'idRegion'); // areas-region
        $this->addForeignKey('debtsToConsumer', 'debts', 'idConsumer', 'consumer','idConsumer'); // users-person
    }

    public function down()
    {
        $this->dropTable('benefit');
        $this->dropTable('clietns');
        $this->dropTable('persons');
        $this->dropTable('areas');
        $this->dropTable('regions');
        $this->dropTable('cities');
        $this->dropTable('streets');
        $this->dropTable('houses');
        $this->dropTable('apartments');
        $this->dropTable('consumer');
        $this->dropTable('tariff');
        $this->dropTable('providers');
        $this->dropTable('debts');
        $this->dropTable('order');
        $this->dropForeignKey('clientToBenefit', 'clients');//, 'idBenefit'), 'benefit', 'idBenefit'); // clients-benefit
        $this->dropForeignKey('clientToPerson', 'clients');//, 'idPerson', 'persons', 'idPerson'); // clients-person
        $this->dropForeignKey('consumerToClients', 'consumer');//, 'idClient', 'clients', 'idClient'); // consumer-clients
        $this->dropForeignKey('consumerToApartments', 'consumer');//, 'idApartment', 'apartments', 'idApartment'); // consumer-apartment
        $this->dropForeignKey('consumerToProviders', 'consumer');//, 'idProvider', 'providers', 'idProvider'); // consumer-provider
        $this->dropForeignKey('consumerToOrder', 'order');//, 'idOrder', 'order', 'idOrder'); // consumer-order
        $this->dropForeignKey('consumerToTariff', 'consumer');//, 'idTariff', 'tariff', 'idTariff'); // consumer-tariff
        $this->dropForeignKey('apartmentToHouses', 'apartments');//, 'idHouse', 'houses', 'idHouse'); // apartment-houses
        $this->dropForeignKey('housesToStreets', 'houses');//, 'idStreet');//, 'streets', 'idStreet'); // houses-streets
        $this->dropForeignKey('streetsToCities', 'streets');//, 'idCity', 'cities', 'idCity'); // streets-cities
        $this->dropForeignKey('citiesToAreas', 'cities');//, 'idArea', 'areas', 'idArea'); // cities-areas
        $this->dropForeignKey('areasToRegions', 'areas');//, 'idRegion', 'regions', 'idRegion'); // areas-region

        $this->dropForeignKey('debtsToConsumer', 'debts');//, 'idConsumer', 'consumer','idConsumer'); // users-person
    }
}
