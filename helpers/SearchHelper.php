<?php

namespace app\helpers;

use Yii;

class SearchHelper
{
    public static  function translit($search) {
        $old = ['й', 'ц', 'у', 'к', 'е', 'н', 'г', 'ш', 'щ', 'з', /*'х', 'ъ',*/ 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'я', 'ч', 'с', 'м', 'и', 'т', 'ь', 'б', 'ю', ' '];
        $new = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', /*'[', ']',*/ 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', ' '];

        return str_ireplace($old, $new, $search);
    }

    public static function correct($search, $tables)
    {
        /*if (preg_match("/[а-яА-ЯёЁ]+$/", $search)) {
            var_dump(self::translit($search));exit;
        }*/
        $min = 1000;
        $result = null;
        foreach($tables as $table) {
            $temp = levenshtein($search, $table);
            if ($min > $temp) {
                $min = $temp;
                $result = $table;
            }
        }
        if ($min > 2) {
            return false;
        }
        return $result;

    }
}