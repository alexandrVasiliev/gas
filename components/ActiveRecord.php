<?php

namespace app\components;

class ActiveRecord extends \yii\db\ActiveRecord
{
    public function alertError($message)
    {
        print "<script language=\"javascript\" type=\"text/javascript\">";
        print "alert('".$message."');";
        print "window.location = \"javascript:history.back();\"";
        print "</script>";
    }
}