<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;

class MenuWidget extends Widget
{
    public $data = [];

    public function init()
    {
        parent::init();

        $query = (new Query)->select(['count(*) as count, "clients" as entity'])->from('clients')
            ->union((new Query)->select(['count(*) as count, "persons" as entity'])->from('persons'))
            ->union((new Query)->select(['count(*) as count, "order" as entity'])->from('order')->where(['status' => 1]))
            ->union((new Query)->select(['count(*) as count, "consumer" as entity'])->from('consumer')->where(['status' => 1]));;

        if (\Yii::$app->user->can('admin')) {
            $query->union((new Query)->select(['count(*) as count, "providers" as entity'])->from('providers')->where(['status' => 1]));
            $query->union((new Query)->select(['count(*) as count, "debts" as entity'])->from('debts'));
            $query->union((new Query)->select(['count(*) as count, "apartments" as entity'])->from('apartments')->where(['status' => 1]));
            $query->union((new Query)->select(['count(*) as count, "houses" as entity'])->from('houses')->where(['status' => 1]));
            $query->union((new Query)->select(['count(*) as count, "streets" as entity'])->from('streets')->where(['status' => 1]));
            $query->union((new Query)->select(['count(*) as count, "cities" as entity'])->from('cities')->where(['status' => 1]));
            $query->union((new Query)->select(['count(*) as count, "areas" as entity'])->from('providers')->where(['status' => 1]));
            $query->union((new Query)->select(['count(*) as count, "regions" as entity'])->from('regions')->where(['status' => 1]));
            $query->union((new Query)->select(['count(*) as count, "benefit" as entity'])->from('benefit')->where(['status' => 1]));
            $query->union((new Query)->select(['count(*) as count, "tariff" as entity'])->from('tariff'));
            $query->union((new Query)->select(['count(*) as count, "consumer" as entity'])->from('consumer')->where(['status' => 1]));
            $query->union((new Query)->select(['count(*) as count, "user" as entity'])->from('user'));
        }
        $this->data = $query->all();

    }

    public function run()
    {
        $str = "<ul class='list-group'>";

        if (\Yii::$app->user->can('admin')) {
            $str .= "<li class='list-group-item'>".Html::a('Statistics', '/admin/consumption/tree')."</li>";
            for ($i=4; $i<15; $i++) {
                $str .= "<li class='list-group-item'><span class='badge'>".$this->data[$i]['count']."</span>".Html::a(ucwords($this->data[$i]['entity']), '/admin/'.$this->data[$i]['entity'])."</li>";
            }
        }

        for ($i=0; $i<4; $i++) {
            $str .= "<li class='list-group-item'><span class='badge'>".$this->data[$i]['count']."</span>".Html::a(ucwords($this->data[$i]['entity']), '/admin/'.$this->data[$i]['entity'])."</li>";
        }

        return Html::decode($str);
    }
}