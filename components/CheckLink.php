<?php

namespace app\components;

use yii\base\Behavior;

class CheckLink extends Behavior {

    public $className;
    public $value1;

    public function isLink($id)
    {
        $elements = (new $this->className)->find()->where([$this->value1 => $id])->andWhere(['status' => 1])->all();
        return (count($elements) == 0)? true : false;
    }
}