<?php

namespace app\components;

use yii\base\Behavior;

class ListBehavior extends Behavior
{
    public $className;
    public $value1; // id in where
    public $value2; // options value
    public $value3; // options

    public function getOptionsList($id)
    {
        $elements = (new $this->className)->find()
            ->where([$this->value1 => $id])
            ->andWhere(['status' => 1])
            ->all();
        if ($elements){
            foreach($elements as $element) {
                print "<option value=".$element[$this->value2].">".$element[$this->value3]."</option>";
            }
        } else {
            print  "<option>not found</option>";
        }
    }

}