<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CitiesSearch represents the model behind the search form about `app\module\admin\models\Cities`.
 */
class CitiesSearch extends Cities
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCity'], 'integer'],
            [['city', 'idArea'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cities::find()->where(['cities.status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('area');

        $query->andFilterWhere([
            'idCity' => $this->idCity,
            'areas.area' => $this->idArea,
        ]);

        $query->andFilterWhere(['like', 'city', $this->city]);

        return $dataProvider;
    }

    public static function searchCities($search)
    {
        $cities = Cities::find()
            ->select('idCity, city, area, region')
            ->innerJoin('areas', 'cities.idArea = areas.idArea')
            ->innerJoin('regions', 'regions.idRegion = areas.idRegion')
            ->where(['like', 'city', $search])
            ->orWhere(['like', 'cities.idCity', $search])
            ->andWhere(['cities.status' => 1])
            ->asArray()->all();

        foreach($cities as &$city) {
            $city['type'] = 'city';
        }

        return $cities;
    }
}
