<?php

namespace app\module\admin\models;

use Yii;
use yii\db\Query;
use app\components\CheckLink;
use app\components\ActiveRecord;

/**
 * This is the model class for table "persons".
 *
 * @property integer $idPerson
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 *
 * @property Clients[] $clients
 * @property Users[] $users
 */
class Persons extends ActiveRecord
{

    public function behaviors()
    {
        return [
            'isLink' =>
                [
                    'class' => CheckLink::className(),
                    'className' => Clients::className(),
                    'value1' => 'idPerson',
                ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'middlename', 'lastname'], 'required'],
            [['firstname', 'middlename', 'lastname'], 'string', 'max' => 15],
            [['firstname', 'middlename', 'lastname'], 'uniques', 'on' => 'create'],

        ];
    }

    public function uniques($attribute, $params)
    {
        $persons = (new Query)->select('*')
            ->from('persons')
            ->where([
                    'firstname' => $this->firstname,
                    'middlename' => $this->middlename,
                    'lastname' => $this->lastname,
                ]
            )->exists();
        if ($persons) {
            $this->addError($attribute, 'Person already exist');
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPerson' => 'Id Person',
            'firstname' => 'Firstname',
            'middlename' => 'Middlename',
            'lastname' => 'Lastname',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getAllPersons()
    {
        return $this->find()->all();
    }

    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['idPerson' => 'idPerson']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['idPerson' => 'idPerson']);
    }

}
