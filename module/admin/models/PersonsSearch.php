<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\admin\models\Persons;

/**
 * PersonsSearch represents the model behind the search form about `app\module\admin\models\Persons`.
 */
class PersonsSearch extends Persons
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPerson'], 'integer'],
            [['firstname', 'middlename', 'lastname'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Persons::find()->where(['status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idPerson' => $this->idPerson,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'middlename', $this->middlename])
            ->andFilterWhere(['like', 'lastname', $this->lastname]);


        return $dataProvider;
    }

    public static function searchPersons($search)
    {
        $persons = Persons::find()
            ->select('persons.idPerson, firstname, middlename, lastname')
            ->innerJoin('clients', 'clients.idPerson = persons.idPerson')
            ->where(['like', 'firstname', $search])
            ->orWhere(['like', 'lastname', $search])
            ->orWhere(['like', 'lastname', $search])
            ->orWhere(['like', 'personalAccount', $search])
            ->orWhere(['like', 'persons.idPerson', $search])
            ->andWhere(['persons.status' => 1])
            ->asArray()->all();

        foreach($persons as &$person) {
            $person['type'] = 'person';
        }

        return $persons;
    }

}
