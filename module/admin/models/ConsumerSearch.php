<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ConsumerSearch represents the model behind the search form about `app\module\admin\models\Consumer`.
 */
class ConsumerSearch extends Consumer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idConsumer', 'idTariff', 'idApartment', 'idClient'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Consumer::find()->where(['status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idConsumer' => $this->idConsumer,
            'idTariff' => $this->idTariff,
            //'idOrder' => $this->idOrder,
            'idApartment' => $this->idApartment,
            'idClient' => $this->idClient,
        ]);

        return $dataProvider;
    }

    public static function searchConsumers($search)
    {
        $consumers = Consumer::find()
            ->select('idConsumer, name')
            ->where(['like', 'name', $search])
            ->orWhere(['like', 'idConsumer', $search])
            ->andWhere(['consumer.status' => 1])
            ->asArray()->all();

        foreach($consumers as &$consumer) {
            $consumer['type'] = 'consumer';
        }

        return $consumers;
    }
}
