<?php

namespace app\module\admin\models;

use yii\db\Query;
use Yii;
use app\components\ActiveRecord;
use app\components\CheckLink;

/**
 * This is the model class for table "apartments".
 *
 * @property integer $idApartment
 * @property string $apartment
 * @property integer $idHouse
 *
 * @property Houses $idHouse0
 * @property Consumer[] $consumers
 */
class Apartments extends ActiveRecord
{
    public $idStreet;
    public $idRegion;
    public $idArea;
    public $idCity;

    public function behaviors()
    {
        return [
            'isLink' =>
                [
                    'class' => CheckLink::className(),
                    'className' => Consumer::className(),
                    'value1' => 'idApartment',
                ]
        ];
    }

    public static function tableName()
    {
        return 'apartments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idHouse', 'apartment'], 'required'],
            [['idHouse'], 'integer'],
            [['apartment'], 'string', 'max' => 20],
            ['apartment', 'uniqueApartmentInHouse', 'on' => 'create'],
        ];
    }

    public function uniqueApartmentInHouse($attribute, $param)
    {
        if (in_array($this->$attribute, (new Apartments())->getApartmentValues($this->idHouse))) {
            $this->addError($attribute, 'Such apartment exists in the house');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idApartment' => 'Id Apartment',
            'apartment' => 'Apartment',
            'idHouse' => 'House',
            'idStreet' => 'Street',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouse()
    {
        return $this->hasOne(Houses::className(), ['idHouse' => 'idHouse']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getConsumers()
    {
        return $this->hasMany(Consumer::className(), ['idApartment' => 'idApartment']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getAllApartments()
    {
        return $this->find()->where(['status' => 1])->all();
    }


    public static function getApartmentsByDate($id, $timeStart, $timeEnd) // get apartments from .. to
    {
        return (new Query)->select('apartments.idApartment, apartments.apartment, apartments.idHouse')
            ->from('apartments')
            ->innerJoin('consumer', 'apartments.idApartment = consumer.idApartment')
            ->innerJoin('order', 'consumer.idConsumer = order.idConsumer')
            ->where(['apartments.idHouse' => $id])
            ->andWhere('date between :start and :end')
            ->andWhere(['apartments.status' => 1])
            ->params([':start' => $timeStart, ':end' => $timeEnd])
            ->groupBy('apartments.idApartment')
            ->all();
    }

    public static function getKvByApartment($id, $newDate) // get used by apartment
    {
        return Consumption::getData('order', $id, $newDate, 'apartments.idApartment', 'apartments.apartment', Consumption::APARTMENT);
    }


    public static function getDebtByApartment($id, $newDate) // get debts by apartment
    {
        return Consumption::getData('debts', $id, $newDate, 'apartments.idApartment', 'apartments.apartment', Consumption::APARTMENT);
    }

    public static function getApartmentValues($id)
    {
        $query = (new Query())->select('apartment')->from('apartments')->where(['idHouse' => $id])->all();
        $result = [];
        foreach($query as $apartment) {
            $result[] = $apartment['apartment'];
        }
        return $result;
    }

}
