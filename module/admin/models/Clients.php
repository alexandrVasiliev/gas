<?php

namespace app\module\admin\models;

use Yii;
use app\components\ActiveRecord;
use app\components\CheckLink;

/**
 * This is the model class for table "clients".
 *
 * @property integer $idClient
 * @property integer $idPerson
 * @property integer $idBenefit
 * @property string $personalAccount
 *
 * @property Persons $idPerson0
 * @property Benefit $idBenefit0
 * @property Consumer[] $consumers
 */
class Clients extends ActiveRecord
{

    public function behaviors()
    {
        return [
            'isLink' =>
                [
                    'class' => CheckLink::className(),
                    'className' => Consumer::className(),
                    'value1' => 'idClient',
                ]
        ];
    }
    public $firstname;
    public $middlename;
    public $lastname;
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idBenefit', 'personalAccount'], 'required'],
            [['idPerson', 'idBenefit'], 'integer'],
            ['idPerson', 'unique'],
            [['personalAccount'], 'string', 'max' => 11],
            [['personalAccount'], 'string', 'min' => 11],
            ['personalAccount', 'unique'],
            [['firstname', 'middlename', 'lastname'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idClient' => 'Id Client',
            'idPerson' => 'Person',
            'idBenefit' => 'Benefit',
            'personalAccount' => 'Personal Account',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Persons::className(), ['idPerson' => 'idPerson']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getBenefit()
    {
        return $this->hasOne(Benefit::className(), ['idBenefit' => 'idBenefit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsumers()
    {
        return $this->hasMany(Consumer::className(), ['idClient' => 'idClient']);
    }

    public function getAllClients()
    {
        return Clients::find()->where(['status' => 1])->all();
    }
}
