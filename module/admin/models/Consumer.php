<?php

namespace app\module\admin\models;

use Yii;
use app\components\ActiveRecord;
use app\components\CheckLink;

/**
 * This is the model class for table "consumer".
 *
 * @property integer $idConsumer
 * @property integer $idTariff
 * @property integer $idProvider
 * @property integer $idApartment
 * @property integer $idClient
 *
 * @property Tariff $idTariff0
 * @property Apartments $idApartment0
 * @property Clients $idClient0
 * @property Debts[] $debts
 */
class Consumer extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'isLink' =>
                [
                    'class' => CheckLink::className(),
                    'className' => Order::className(),
                    'value1' => 'idConsumer',
                ]
        ];
    }
    /**
     * @inheritdoc
     */
    public $idHouse;
    public $idStreet;

    public static function tableName()
    {
        return 'consumer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idTariff', 'idApartment', 'idClient'], 'required'],
            [['idTariff', 'idApartment', 'idClient'], 'integer'],
            [['idHouse', 'idStreet'],'safe'],
            ['idApartment', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idConsumer' => 'Id Consumer',
            'idTariff' => 'Select tariff',
            'idApartment' => 'Select Apartment',
            'idClient' => 'Select a client',
            'idStreet' => 'Select street',
            'idHouse' => 'Select # house'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['idTariff' => 'idTariff']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartment()
    {
        return $this->hasOne(Apartments::className(), ['idApartment' => 'idApartment']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['idClient' => 'idClient']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDebts()
    {
        return $this->hasMany(Debts::className(), ['idConsumer' => 'idConsumer']);
    }

    public function getAllConsumers() // get active consumers
    {
        return $this->find()->where(['status' => 1])->all();
    }

    public function getLastIndex() // get last idConsumer
    {   
        $id = $this->find()->orderBy(['idConsumer' => SORT_DESC])->one();
        return $id->idConsumer;
    }

}
