<?php

namespace app\module\admin\models;

use yii\db\Query;
use Yii;
use app\components\ListBehavior;
use app\components\ActiveRecord;
use app\components\CheckLink;

/**
 * This is the model class for table "streets".
 *
 * @property integer $idStreet
 * @property string $street
 * @property integer $idCity
 *
 * @property Houses[] $houses
 * @property Cities $idCity0
 */
class Streets extends ActiveRecord
{

    public function behaviors()
    {
        return [
            'List' =>
                [
                    'class' => ListBehavior::className(),
                    'className' => Streets::className(),
                    'value1' => 'idCity',
                    'value2' => 'idStreet',
                    'value3' => 'street',
                ],
            'isLink' =>
                [
                    'class' => CheckLink::className(),
                    'className' => Houses::className(),
                    'value1' => 'idStreet',
                ]
        ];
    }
    /**
     * @inheritdoc
     */
    public $idArea;
    public $idRegion;
    public static function tableName()
    {
        return 'streets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCity', 'street'], 'required'],
            [['idCity'], 'integer'],
            [['street'], 'string', 'max' => 255],
            ['street', 'uniqueStreetInCity', 'on' => 'create'],
        ];
    }

    public function uniqueStreetInCity($attribute, $param)
    {
        if (in_array($this->$attribute, (new Streets())->getStreetValues($this->idCity))) {
            $this->addError($attribute, 'Such street exists in the city');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idStreet' => 'Id Street',
            'street' => 'Street',
            'idCity' => 'City',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHouses()
    {
        return $this->hasMany(Houses::className(), ['idStreet' => 'idStreet']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['idCity' => 'idCity']);
    }

    public static function getStreets($id, $timeStart, $timeEnd) // get street from .. to
    {
        return (new Query)->select('streets.idStreet, street, streets.idCity')
            ->from('streets')
            ->innerJoin('houses', 'streets.idStreet = houses.idStreet')
            ->innerJoin('apartments', 'houses.idHouse = apartments.idHouse')
            ->innerJoin('consumer', 'apartments.idApartment = consumer.idApartment')
            ->innerJoin('order', 'consumer.idConsumer = order.idConsumer')
            ->where(['streets.idCity' => $id])
            ->andWhere('date between :start and :end')
            ->andWhere(['streets.status' => 1])
            ->params([':start' => $timeStart, ':end' => $timeEnd])
            ->groupBy('streets.idStreet')
            ->all();
    }

    public function getAllStreets() // get active streets
    {
        return $this->find()->where(['status' => 1])->all();
    }

    public static function getStreetValues($id)
    {
        $query = (new Query())->select('street')->from('streets')->where(['idCity' => $id])->all();
        $result = [];
        foreach($query as $street) {
            $result[] = $street['street'];
        }
        return $result;
    }

    public static function getKvByStreet($id, $newDate) // get used by street
    {
        return Consumption::getData('order', $id, $newDate, 'streets.idStreet', 'streets.street', Consumption::STREET);
    }


    public static function getDebtsByStreet($id, $newDate) // get debt by street
    {
        return Consumption::getData('debts', $id, $newDate, 'streets.idStreet', 'streets.street', Consumption::STREET);
    }

}
