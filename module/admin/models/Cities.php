<?php

namespace app\module\admin\models;

use yii\db\Query;
use Yii;
use app\components\ListBehavior;
use app\components\ActiveRecord;
use app\components\CheckLink;

/**
 * This is the model class for table "cities".
 *
 * @property integer $idCity
 * @property string $city
 * @property integer $idArea
 *
 * @property Areas $idArea0
 * @property Streets[] $streets
 */
class Cities extends ActiveRecord
{

    public function behaviors()
    {
        return [
            'List' =>
                [
                    'class' => ListBehavior::className(),
                    'className' => Streets::className(),
                    'value1' => 'idCity',
                    'value2' => 'idStreet',
                    'value3' => 'street',
                ],
            'isLink' =>
                [
                    'class' => CheckLink::className(),
                    'className' => Streets::className(),
                    'value1' => 'idCity',
                ]
        ];
    }
    public $idRegion;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city', 'idArea'], 'required'],
            [['idArea'], 'integer'],
            [['city'], 'string', 'max' => 50],
            ['city', 'uniqueCityInArea', 'on' => 'create'],
        ];
    }

    public function uniqueCityInArea($attribute, $param)
    {
        if (in_array($this->$attribute, (new Cities())->getCityValues($this->idArea))) {
            $this->addError($attribute, 'Such city exists in the area');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCity' => 'Id City',
            'city' => 'City',
            'idArea' => 'Area',
            'idRegion' => 'Region',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(Areas::className(), ['idArea' => 'idArea']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreets()
    {
        return $this->hasMany(Streets::className(), ['idCity' => 'idCity']);
    }

    public static function getCities($id, $timeStart, $timeEnd) // get cities from .. to
    {
        return (new Query)->select('cities.idCity, city, idArea')
            ->from('cities')
            ->innerJoin('streets', 'cities.idCity = streets.idCity')
            ->innerJoin('houses', 'streets.idStreet = houses.idStreet')
            ->innerJoin('apartments', 'houses.idHouse = apartments.idHouse')
            ->innerJoin('consumer', 'apartments.idApartment = consumer.idApartment')
            ->innerJoin('order', 'consumer.idConsumer = order.idConsumer')
            ->where(['cities.idArea' => $id])
            ->andWhere('date between :start and :end')
            ->andWhere(['cities.status' => 1])
            ->params([':start' => $timeStart, ':end' => $timeEnd])
            ->groupBy('cities.idCity')
            ->all();
    }

    public function getAllCities() // get active cities
    {
        return $this->find()->where(['status' => 1])->all();
    }


    public static function getKvByCity($id, $newDate) // get used by city
    {
        return Consumption::getData('order', $id, $newDate, 'cities.idCity', 'cities.city', Consumption::CITY);
    }

    public static function getDebtsByCity($id, $newDate) // get debt by city
    {
        return Consumption::getData('debts', $id, $newDate, 'cities.idCity', 'cities.city', Consumption::CITY);
    }

    public static function getCityValues($id)
    {
        $query = (new Query())->select('city')->from('cities')->where(['idArea' => $id])->all();
        $result = [];
        foreach($query as $city) {
            $result[] = $city['city'];
        }
        return $result;
    }

}
