<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\admin\models\Debts;

/**
 * DebtsSearch represents the model behind the search form about `app\module\admin\models\Debts`.
 */
class DebtsSearch extends Debts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idDebt', 'currentIndexAuto', 'unpaidCurrentMonth'], 'integer'],
            [['date', 'idConsumer'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Debts::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('consumer');

        $query->andFilterWhere([
            'idDebt' => $this->idDebt,
            'currentIndexAuto' => $this->currentIndexAuto,
            'date' => $this->date,
            'unpaidCurrentMonth' => $this->unpaidCurrentMonth,
            'consumer.name' => $this->idConsumer,
        ]);

        return $dataProvider;
    }
}
