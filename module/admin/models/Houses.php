<?php

namespace app\module\admin\models;

use yii\db\Query;
use Yii;
use app\components\ListBehavior;
use app\components\ActiveRecord;
use app\components\CheckLink;

/**
 * This is the model class for table "houses".
 *
 * @property integer $idHouse
 * @property string $house
 * @property integer $idStreet
 *
 * @property Apartments[] $apartments
 * @property Streets $idStreet0
 */
class Houses extends ActiveRecord
{

    public function behaviors()
    {
        return [
            'List' =>
                [
                    'class' => ListBehavior::className(),
                    'className' => Houses::className(),
                    'value1' => 'idStreet',
                    'value2' => 'idHouse',
                    'value3' => 'house',
                ],
            'isLink' =>
                [
                    'class' => CheckLink::className(),
                    'className' => Apartments::className(),
                    'value1' => 'idHouse',
                ]
        ];
    }

    public $idRegion;
    public $idArea;
    public $idCity;
    public static function tableName()
    {
        return 'houses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idStreet', 'house'], 'required'],
            [['idStreet'], 'integer'],
            [['house'], 'string', 'max' => 255],
            ['house', 'uniqueHouseInStreet', 'on' => 'create']
        ];
    }

    public function uniqueHouseInStreet($attribute, $param)
    {
        if (in_array($this->$attribute, (new Houses())->getHouseValues($this->idStreet))) {
            $this->addError($attribute, 'Such house exists in the street');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idHouse' => 'Id House',
            'house' => 'House',
            'idStreet' => 'Street',
            'idCity' => 'City',
            'idArea' => 'Area',
            'idRegion' => 'Region',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartments()
    {
        return $this->hasMany(Apartments::className(), ['idHouse' => 'idHouse']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreet()
    {
        return $this->hasOne(Streets::className(), ['idStreet' => 'idStreet']);
    }

    public static function getHouses($id, $timeStart, $timeEnd) // get house from .. to
    {
        return (new Query)->select('houses.idHouse, houses.house, houses.idStreet')
            ->from('houses')
            ->innerJoin('apartments', 'houses.idHouse = apartments.idHouse')
            ->innerJoin('consumer', 'apartments.idApartment = consumer.idApartment')
            ->innerJoin('order', 'consumer.idConsumer = order.idConsumer')
            ->where(['houses.idStreet' => $id])
            ->andWhere('date between :start and :end')
            ->andWhere(['houses.status' => 1])
            ->params([':start' => $timeStart, ':end' => $timeEnd])
            ->groupBy('houses.idHouse')
            ->all();
    }

    public function getAllHouses() // get active houses
    {
        return $this->find()->where(['status' => 1])->all();
    }

    public static function getKvByCity($id, $newDate) // get used by house
    {
        return Consumption::getData('order', $id, $newDate, 'houses.idHouse', 'houses.house', Consumption::HOUSE);
    }

    public static function getDebtsByHouse($id, $newDate) // get debt by house
    {
        return Consumption::getData('debts', $id, $newDate, 'houses.idHouse', 'houses.house', Consumption::HOUSE);
    }

    public static function getHouseValues($id)
    {
        $query = (new Query())->select('house')->from('houses')->where(['idStreet' => $id])->all();
        $result = [];
        foreach($query as $house) {
            $result[] = $house['house'];
        }
        return $result;
    }

}
