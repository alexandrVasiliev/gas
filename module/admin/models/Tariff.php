<?php

namespace app\module\admin\models;

use Yii;

/**
 * This is the model class for table "tariff".
 *
 * @property integer $idTariff
 * @property double $tariffAfter250
 * @property double $tariffUpTo250
 *
 * @property Consumer[] $consumers
 */
class Tariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariffAfter250', 'tariffUpTo250', 'name'], 'required'],
            [['tariffAfter250', 'tariffUpTo250'], 'number'],
            [['name'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idTariff' => 'Id Tariff',
            'tariffAfter250' => 'Tariff After 250',
            'tariffUpTo250' => 'Tariff Up To 250',
        ];
    }

    public function getAllTariff()
    {
        return (new Tariff())->find()->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsumers()
    {
        return $this->hasMany(Consumer::className(), ['idTariff' => 'idTariff']);
    }
}
