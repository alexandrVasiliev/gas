<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\admin\models\Streets;

/**
 * StreetsSearch represents the model behind the search form about `app\module\admin\models\Streets`.
 */
class StreetsSearch extends Streets
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idStreet'], 'integer'],
            [['street', 'idCity'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Streets::find()->where(['streets.status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('city');

        $query->andFilterWhere([
            'idStreet' => $this->idStreet,
            'cities.city' => $this->idCity,
        ]);

        $query->andFilterWhere(['like', 'street', $this->street]);

        return $dataProvider;
    }

    public static function searchStreets($search)
    {
        $streets = Streets::find()
            ->select('idStreet, street, city, area, region')
            ->innerJoin('cities', 'cities.idCity = streets.idCity')
            ->innerJoin('areas', 'cities.idArea = areas.idArea')
            ->innerJoin('regions', 'regions.idRegion = areas.idRegion')
            ->where(['like', 'street', $search])
            ->orWhere(['like', 'streets.idStreet', $search])
            ->andWhere(['streets.status' => 1])
            ->asArray()->all();

        foreach($streets as &$street) {
            $street['type'] = 'street';
        }

        return $streets;
    }
}
