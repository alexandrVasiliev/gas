<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\admin\models\Providers;

/**
 * ProvidersSearch represents the model behind the search form about `app\module\admin\models\Providers`.
 */
class ProvidersSearch extends Providers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idProvider', 'kc'], 'integer'],
            [['pr', 'mfo', 'edrpoy'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Providers::find()->where(['status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idProvider' => $this->idProvider,
            'kc' => $this->kc,
        ]);

        $query->andFilterWhere(['like', 'pr', $this->pr])
            ->andFilterWhere(['like', 'mfo', $this->mfo])
            ->andFilterWhere(['like', 'edrpoy', $this->edrpoy]);

        return $dataProvider;
    }

    public static function searchProviders($search)
    {
        $providers = Providers::find()
            ->select('idProvider, name')
            ->where(['like', 'name', $search])
            ->orWhere(['like', 'kc', $search])
            ->orWhere(['like', 'pr', $search])
            ->orWhere(['like', 'mfo', $search])
            ->orWhere(['like', 'edrpoy', $search])
            ->orWhere(['like', 'providers.idProvider', $search])
            ->andWhere(['status' => 1])
            ->asArray()->all();

        foreach($providers as &$provider) {
            $provider['type'] = 'provider';
        }

        return $providers;
    }
}
