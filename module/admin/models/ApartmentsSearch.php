<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\admin\models\Apartments;

/**
 * ApartmentsSearch represents the model behind the search form about `app\module\admin\models\Apartments`.
 */
class ApartmentsSearch extends Apartments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['idHouse', 'apartment'], 'required'],
            [['idApartment'], 'integer'],
            [['apartment', 'idHouse'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Apartments::find()->where(['apartments.status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('house');

        $query->andFilterWhere([
            'idApartment' => $this->idApartment,
//            'idHouse' => $this->idHouse,
            'houses.house' => $this->idHouse,
        ]);

        $query->andFilterWhere(['like', 'apartment', $this->apartment]);

        return $dataProvider;
    }

    public static function searchApartments($search)
    {
        $apartments = Apartments::find()
            ->select('idApartment, apartment, house, street, city, area, region')
            ->innerJoin('houses', 'houses.idHouse = apartments.idHouse')
            ->innerJoin('streets', 'houses.idStreet = streets.idStreet')
            ->innerJoin('cities', 'cities.idCity = streets.idCity')
            ->innerJoin('areas', 'cities.idArea = areas.idArea')
            ->innerJoin('regions', 'regions.idRegion = areas.idRegion')
            ->where(['like', 'apartment', $search])
            ->orWhere(['like', 'apartments.idApartment', $search])
            ->andWhere(['apartments.status' => 1])
            ->asArray()->all();

        foreach($apartments as &$apartment) {
            $apartment['type'] = 'apartment';
        }

        return $apartments;
    }
}
