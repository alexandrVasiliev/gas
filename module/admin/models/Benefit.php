<?php

namespace app\module\admin\models;

use Yii;
use app\components\ActiveRecord;
use app\components\CheckLink;

/**
 * This is the model class for table "benefit".
 *
 * @property integer $idBenefit
 * @property double $benefit
 * @property string $nameBenefit
 * @property integer $limit
 *
 * @property Clients[] $clients
 */
class Benefit extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'isLink' =>
                [
                    'class' => CheckLink::className(),
                    'className' => Clients::className(),
                    'value1' => 'idBenefit',
                ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'benefit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['benefit', 'nameBenefit', 'limit'], 'required'],
            [['benefit'], 'number'],
            [['limit'], 'integer'],
            [['nameBenefit'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idBenefit' => 'Id Benefit',
            'benefit' => 'Benefit',
            'nameBenefit' => 'Type',
            'limit' => 'Limit',
        ];
    }

    public function getAllBenefits()
    {
        return $this->find()->where(['status' => 1])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['idBenefit' => 'idBenefit']);
    }
}
