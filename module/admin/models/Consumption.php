<?php

namespace app\module\admin\models;

use Yii;
use DateTime;
use alexgx\phpexcel;
use PHPExcel_Writer_Excel5;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
use app\components\ActiveRecord;

/**
 * This is the model class for table "apartments".
 */

class Consumption extends ActiveRecord
{
    const REGION = 1;
    const AREA = 2;
    const CITY = 3;
    const STREET = 4;
    const HOUSE = 5;
    const APARTMENT = 6;

    public function findDate($period, $interval, $timeStart, $timeEnd)
    {
        $newDate = [];
        $maxDate = (date('Y-m-d') > $timeEnd)? $timeEnd : date('Y-m-d');
        $modifyOne = null;
        $modifyTwo = null;
        $date = new DateTime(date('Y-m-d', strtotime($timeStart)));

        if ($period == 1) { // days
            $value = ($interval->format('%a'))? $interval->format('%a') : 1;
            $modifyOne = "+0 day";
            $modifyTwo = "+1 day";

            for ($i=1; $i<$value+1; $i++) {
                //if ($i === 2 && $modifyOne == '+0 day') {
//                    $modifyOne = '+1 day';
                //}
                $first = $date->modify($modifyOne)->format('Y-m-d');
                $last = $date->modify($modifyTwo)->format('Y-m-d');

                if ($date->format('Y-m-d') > $maxDate || $first > $maxDate || $last > $maxDate) {
                    break;
                }
                $newDate[] = $first;
                $newDate[] = $last;
            }

            return $newDate;

        } else if ($period == 2) { // month
            $value = ($interval->format('%y')*12 + $interval->format('%m'))? ($interval->format('%y')*12 + $interval->format('%m')): 0;
            $value += ($interval->format('%d'))? 1 : 0;
            $modifyOne = "+0 day";
            $modifyTwo = "+1 month";

        } else if ($period == 3) { // 3 month
            $value = ceil(($interval->format('%y')*12 + $interval->format('%m'))/3);
            if (($interval->format('%m') % 3 == 0) and ($interval->format('%d') > 0)) {
                $value++;
            }
            $modifyOne = '+0 day';
            $modifyTwo = '+3 month';

        } else if ($period == 4){ // year
            $value = ($interval->format('%a')/365)? ceil($interval->format('%a')/365) : 1;
            $modifyOne = '+0 day';
            $modifyTwo = '+1 year';

        } else if ($period == 5) { // month lida

            $value = ($interval->format('%y')*12 + $interval->format('%m'))? ($interval->format('%y')*12 + $interval->format('%m')): 0;
            $value += ($interval->format('%d'))? 1 : 0;
            $newDate[] = $date->format('Y-m-d');
            $modifyOne = 'last day of 0 month';
            $modifyTwo = 'first day of +1 month';
            for ($i=1; $i<$value+2; $i++) {
                if ($i === 2 && $modifyOne == '+0 day') {
                    $modifyOne = '+1 day';
                }
                $first = $date->modify($modifyOne)->format('Y-m-d');
                $last = $date->modify($modifyTwo)->format('Y-m-d');

                if ($date->format('Y-m-d') > $maxDate || $first > $maxDate || $last > $maxDate) {
                    $newDate[] = $maxDate;
                    break;
                }
                $newDate[] = $first;
                $newDate[] = $last;
            }

            return $newDate;

        } elseif ($period == 6) {
            $type = 'year';
            $value = ($interval->format('%a')/365)? ceil($interval->format('%a')/365) : 1;
            $newDate[] = $timeStart;
            for($i=1; $i<=$value+1; $i++) {
                $date = new DateTime(date('Y-m-d', strtotime($timeStart." +".($i-1)." ".$type)));

                $year = date('Y', strtotime($date->format('Y-m-d')));
                $lastDayOfYear = date('Y-m-d',strtotime($year.'-12-31'));
                $firstDayOfYear = date('Y-m-d', strtotime(++$year.'-01-01'));
                if ($date->format('Y-m-d') > $maxDate || $lastDayOfYear > $maxDate || $firstDayOfYear > $maxDate) {
                    $newDate[] = $maxDate;
                    break;
                }
                $newDate[] = $lastDayOfYear;
                $newDate[] = $firstDayOfYear;

            }
            return $newDate;
        }

        for ($i=1; $i<$value+1; $i++) {
            if ($i === 2 && $modifyOne == '+0 day') {
                $modifyOne = '+1 day';
            }
            $first = $date->modify($modifyOne)->format('Y-m-d');
            $last = $date->modify($modifyTwo)->format('Y-m-d');

            if ($date->format('Y-m-d') > $maxDate || $first > $maxDate || $last > $maxDate) {
                $newDate[] = $first;
                $newDate[] = $maxDate;
                break;
            }
            $newDate[] = $first;
            $newDate[] = $last;
        }

        return $newDate;
    }

    public function RowColor($sheet, $row, $columnStart, $columnEnd, $color)
    {
        $style = [
            'font' => [
                'name' => 'Arial',
            ],
            'fill' => [
                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                'color' => [
                    'rgb' => $color
                ]
            ],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ],
            'borders' => [
                'outline' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THICK,
                    'color' => [
                        'arbg' => '000000'
                    ],
                ],
            ],
        ];
        for($i=$columnStart; $i<$columnEnd; $i++) {
            $sheet->getStyleByColumnAndRow($i, $row)->applyFromArray($style);
        }
    }

    public function downloadExcel($period = null, $timeStart = null, $timeEnd = null) // create Excel file
    {
        set_time_limit(0);
        $date1 = new DateTime($timeStart);
        $date2 = new DateTime($timeEnd);
        $interval = $date1->diff($date2);
        $newDate = Consumption::findDate($period, $interval, $timeStart,$timeEnd); // dates
        $regions = Regions::getRegions($timeStart, $timeEnd); // regions

        $xls = new \PHPExcel();
        $arr = Regions::getKvByRegion(1, $newDate);

        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $sheet->setTitle('Statistic sheet');
        $sheet->getColumnDimension('A')->setWidth(15);
        $sheet->getColumnDimension('B')->setWidth(15);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(15);
        $sheet->getColumnDimension('E')->setWidth(5);
        $sheet->getColumnDimension('F')->setWidth(5);
        $col = 6 + (count($arr) * 3);
        $j=2; // row number
        foreach($regions as $region) {
            $j++;
            $kvRegion = Regions::getKvByRegion($region->idRegion, $newDate); //used region
            $debtRegion = Regions::getDebtsByRegion($region->idRegion, $newDate); // debt region

            $count = count($kvRegion)*3;
            for($i=0; $i<$count; $i+=3) {
                $valueUsed = ($kvRegion[$i/3]['data'])? $kvRegion[$i/3]['data'] : 0;
                $valueDebt = ($debtRegion[$i/3]['data']) ? $debtRegion[$i/3]['data'] : 0;
                $sheet->setCellValueByColumnAndRow(6+$i, $j, $valueUsed);
                $sheet->setCellValueByColumnAndRow(6+$i+1, $j, $valueDebt);
                $sheet->setCellValueByColumnAndRow(6+$i+2, $j, $valueDebt - $valueUsed);

            }

            $sheet->setCellValueByColumnAndRow(0, $j, $region->region);
            Consumption::RowColor($sheet, $j, 0, $col, 'a1a1a1'); // fill region
            $j++;
            $areas = Areas::getAreas($region->idRegion, $timeStart, $timeEnd);
            foreach($areas as $area) {

                $kvArea = Areas::getKvByArea($area['idArea'], $newDate); //used area
                $debtArea = Areas::getDebtsByArea($area['idArea'], $newDate); // debt area
                for($i=0; $i<$count; $i+=3) {
                    $valueUsed = ($kvArea[$i/3]['data'])? $kvArea[$i/3]['data'] : 0;
                    $valueDebt = ($debtArea[$i/3]['data']) ? $debtArea[$i/3]['data'] : 0;
                    $sheet->setCellValueByColumnAndRow(6+$i, $j, $valueUsed);
                    $sheet->setCellValueByColumnAndRow(6+$i+1, $j, $valueDebt);
                    $sheet->setCellValueByColumnAndRow(6+$i+2, $j, $valueDebt - $valueUsed);

                }

                $sheet->setCellValueByColumnAndRow(1, $j, $area['area']);
                Consumption::RowColor($sheet, $j, 1, $col, 'ffffff'); // fill area
                $j++;
                $cities = Cities::getCities($area['idArea'], $timeStart, $timeEnd);
                foreach($cities as $city) {

                    $kvCity = Cities::getKvByCity($city['idCity'], $newDate); //used city
                    $debtCity = Cities::getDebtsByCity($city['idCity'], $newDate); // debt city
                    for($i=0; $i<$count; $i+=3) {
                        $valueUsed = ($kvCity[$i/3]['data'])? $kvCity[$i/3]['data'] : 0;
                        $valueDebt = ($debtCity[$i/3]['data']) ? $debtCity[$i/3]['data'] : 0;
                        $sheet->setCellValueByColumnAndRow(6+$i, $j, $valueUsed);
                        $sheet->setCellValueByColumnAndRow(6+$i+1, $j, $valueDebt);
                        $sheet->setCellValueByColumnAndRow(6+$i+2, $j, $valueDebt - $valueUsed);

                    }

                    $sheet->setCellValueByColumnAndRow(2, $j, $city['city']);
                    Consumption::RowColor($sheet, $j, 2, $col, 'a1a1a1'); // fill city
                    $j++;
                    $streets = Streets::getStreets($city['idCity'], $timeStart, $timeEnd);
                    foreach($streets as $street) {

                        $kvStreet = Streets::getKvByStreet($street['idStreet'], $newDate); //used street
                        $debtStreet = Streets::getDebtsByStreet($street['idStreet'], $newDate); // debt street
                        for($i=0; $i<$count; $i+=3) {
                            $valueUsed = ($kvStreet[$i/3]['data'])? $kvStreet[$i/3]['data'] : 0;
                            $valueDebt = ($debtStreet[$i/3]['data']) ? $debtStreet[$i/3]['data'] : 0;
                            $sheet->setCellValueByColumnAndRow(6+$i, $j, $valueUsed);
                            $sheet->setCellValueByColumnAndRow(6+$i+1, $j, $valueDebt);
                            $sheet->setCellValueByColumnAndRow(6+$i+2, $j, $valueDebt - $valueUsed);

                        }

                        $sheet->setCellValueByColumnAndRow(3, $j, $street['street']);
                        Consumption::RowColor($sheet, $j, 3, $col, 'ffffff'); // fill street
                        $j++;
                        $houses = Houses::getHouses($street['idStreet'], $timeStart, $timeEnd);
                        foreach($houses as $house) {

                            $kvHouse = Houses::getKvByCity($house['idHouse'], $newDate); //used house
                            $debtHouse = Houses::getDebtsByHouse($house['idHouse'], $newDate); // debt house
                            for($i=0; $i<$count; $i+=3) {
                                $valueUsed = ($kvHouse[$i/3]['data'])? $kvHouse[$i/3]['data'] : 0;
                                $valueDebt = ($debtHouse[$i/3]['data']) ? $debtHouse[$i/3]['data'] : 0;
                                $sheet->setCellValueByColumnAndRow(6+$i, $j, $valueUsed);
                                $sheet->setCellValueByColumnAndRow(6+$i+1, $j, $valueDebt);
                                $sheet->setCellValueByColumnAndRow(6+$i+2, $j, $valueDebt - $valueUsed);

                            }

                            $sheet->setCellValueByColumnAndRow(4, $j, $house['house']);
                            Consumption::RowColor($sheet, $j, 4, $col, 'a1a1a1'); // fill house
                            $j++;
                            $apartments = Apartments::getApartmentsByDate($house['idHouse'], $timeStart, $timeEnd);
                            foreach($apartments as $apartment) {

                                $kvApartment = Apartments::getKvByApartment($apartment['idApartment'], $newDate); //used apartment
                                $debtApartment = Apartments::getDebtByApartment($apartment['idApartment'], $newDate); // debt apartment
                                for($i=0; $i<$count; $i+=3) {
                                    $valueUsed = ($kvApartment[$i/3]['data'])? $kvApartment[$i/3]['data'] : 0;
                                    $valueDebt = ($debtApartment[$i/3]['data']) ? $debtApartment[$i/3]['data'] : 0;
                                    $sheet->setCellValueByColumnAndRow(6+$i, $j, $valueUsed);
                                    $sheet->setCellValueByColumnAndRow(6+$i+1, $j, $valueDebt);
                                    $sheet->setCellValueByColumnAndRow(6+$i+2, $j, $valueDebt - $valueUsed);

                                }

                                $sheet->setCellValueByColumnAndRow(5, $j, $apartment['apartment']);
                                Consumption::RowColor($sheet, $j, 5, $col, 'ffffff'); // fill apartment
                                $j++;
                            }
                        }
                    }
                }
            }
        }
        $k = 6;
        for ($i=0; $i<count($arr); $i++) {
            $sheet->setCellValueByColumnAndRow($i+$k, 1, $arr[$i]['from']);
            $sheet->setCellValueByColumnAndRow($i+$k, 2, 'used');
            $k++;
            $sheet->setCellValueByColumnAndRow($i+$k, 1, "        - ");
            $sheet->setCellValueByColumnAndRow($i+$k, 2, 'index');
            $k++;
            $sheet->setCellValueByColumnAndRow($i+$k, 1, $arr[$i]['to']);
            $sheet->setCellValueByColumnAndRow($i+$k, 2, 'debt');
        }
        ////sheet 2
        $xls->createSheet(null, 1);
        $xls->setActiveSheetIndex(1);
        $sheet2 = $xls->getActiveSheet();
        $sheet2->setTitle('Clients sheet');

        $clients = (new Query)->select('clients.idClient, persons.lastname, persons.middlename, persons.firstname, apartments.apartment, houses.house, streets.street, cities.city, areas.area, regions.region')
            ->from('clients')
            ->innerJoin('persons', 'clients.idPerson = persons.idPerson')
            ->innerJoin('consumer', 'clients.idClient = consumer.idClient')
            ->innerJoin('apartments', 'consumer.idApartment = apartments.idApartment')
            ->innerJoin('houses', 'apartments.idHouse = houses.idHouse')
            ->innerJoin('streets', 'streets.idStreet = houses.idStreet')
            ->innerJoin('cities', 'streets.idCity = cities.idCity')
            ->innerJoin('areas', 'cities.idArea = areas.idArea')
            ->innerJoin('regions', 'areas.idRegion = regions.idRegion')
            ->where(['apartments.status' => 1])
            ->orderBy('persons.lastname, persons.firstname, persons.middlename')
            ->all();

        $j=1;
        $sheet2->getColumnDimension('A')->setWidth(5);
        $sheet2->getColumnDimension('B')->setWidth(15);
        $sheet2->getColumnDimension('C')->setWidth(15);
        $sheet2->getColumnDimension('D')->setWidth(15);
        $sheet2->getColumnDimension('E')->setWidth(10);
        $sheet2->getColumnDimension('F')->setWidth(10);
        $sheet2->getColumnDimension('G')->setWidth(10);
        $sheet2->getColumnDimension('H')->setWidth(10);
        $sheet2->getColumnDimension('I')->setWidth(20);
        $sheet2->getColumnDimension('J')->setWidth(20);

        Consumption::RowColor($sheet2, 1, 0, 10, 'a1a1a1'); // fill street
        $sheet2->setCellValueByColumnAndRow(0, 1, '#');
        $sheet2->setCellValueByColumnAndRow(1, 1, 'FIRSTNAME');
        $sheet2->setCellValueByColumnAndRow(2, 1, 'MIDDLENAME');
        $sheet2->setCellValueByColumnAndRow(3, 1, 'LASTNAME');
        $sheet2->setCellValueByColumnAndRow(4, 1, 'APARTMENT');
        $sheet2->setCellValueByColumnAndRow(5, 1, 'HOUSE');
        $sheet2->setCellValueByColumnAndRow(6, 1, 'STREET');
        $sheet2->setCellValueByColumnAndRow(7, 1, 'CITY');
        $sheet2->setCellValueByColumnAndRow(8, 1, 'AREA');
        $sheet2->setCellValueByColumnAndRow(9, 1, 'REGION');

        foreach($clients as $client) {
            $j++;
            $sheet2->setCellValueByColumnAndRow(0, $j, $j-1);
            $sheet2->setCellValueByColumnAndRow(1, $j, $client['lastname']);
            $sheet2->setCellValueByColumnAndRow(2, $j, $client['middlename']);
            $sheet2->setCellValueByColumnAndRow(3, $j, $client['firstname']);
            $sheet2->setCellValueByColumnAndRow(4, $j, $client['apartment']);
            $sheet2->setCellValueByColumnAndRow(5, $j, $client['house']);
            $sheet2->setCellValueByColumnAndRow(6, $j, $client['street']);
            $sheet2->setCellValueByColumnAndRow(7, $j, $client['city']);
            $sheet2->setCellValueByColumnAndRow(8, $j, $client['area']);
            $sheet2->setCellValueByColumnAndRow(9, $j, $client['region']);

        }
        ///end sheet 2
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $filename = Yii::getAlias('@runtime/report_'.uniqid().'.xls');
        $objWriter->save($filename);
        $result['filename'] = basename($filename);
        echo json_encode($result);
        die;
//        Yii::$app->response->sendFile($filename);
//        unlink($filename);
//        Yii::$app->end();
    }

    public static function buildQuery($entity)
    {
        $result = [];
        if ($entity == self::REGION) {
            $result[] = ['INNER JOIN', 'apartments' , 'apartments.idApartment = consumer.idApartment'];
            $result[] = ['INNER JOIN', 'houses' , 'apartments.idHouse = houses.idHouse'];
            $result[] = ['INNER JOIN', 'streets' , 'houses.idStreet = streets.idStreet'];
            $result[] = ['INNER JOIN', 'cities' , 'streets.idCity = cities.idCity'];
            $result[] = ['INNER JOIN', 'areas' , 'cities.idArea = areas.idArea'];
            $result[] = ['INNER JOIN', 'regions' , 'areas.idRegion = regions.idRegion'];
        } else if ($entity == self::AREA) {
            $result[] = ['INNER JOIN', 'apartments' , 'apartments.idApartment = consumer.idApartment'];
            $result[] = ['INNER JOIN', 'houses' , 'apartments.idHouse = houses.idHouse'];
            $result[] = ['INNER JOIN', 'streets' , 'houses.idStreet = streets.idStreet'];
            $result[] = ['INNER JOIN', 'cities' , 'streets.idCity = cities.idCity'];
            $result[] = ['INNER JOIN', 'areas' , 'cities.idArea = areas.idArea'];
        } else if ($entity == self::CITY) {
            $result[] = ['INNER JOIN', 'apartments' , 'apartments.idApartment = consumer.idApartment'];
            $result[] = ['INNER JOIN', 'houses' , 'apartments.idHouse = houses.idHouse'];
            $result[] = ['INNER JOIN', 'streets' , 'houses.idStreet = streets.idStreet'];
            $result[] = ['INNER JOIN', 'cities' , 'streets.idCity = cities.idCity'];
        } else if ($entity == self::STREET) {
            $result[] = ['INNER JOIN', 'apartments' , 'apartments.idApartment = consumer.idApartment'];
            $result[] = ['INNER JOIN', 'houses' , 'apartments.idHouse = houses.idHouse'];
            $result[] = ['INNER JOIN', 'streets' , 'houses.idStreet = streets.idStreet'];
        } else if ($entity == self::HOUSE) {
            $result[] = ['INNER JOIN', 'apartments', 'apartments.idApartment = consumer.idApartment'];
            $result[] = ['INNER JOIN', 'houses' , 'apartments.idHouse = houses.idHouse'];
        } else if ($entity == self::APARTMENT) {
            $result[] = ['INNER JOIN', 'apartments', 'apartments.idApartment = consumer.idApartment'];
        } else {
            throw new HttpException(500, 'Wrong entity type passed.');
        }

        return $result;
    }

    public function getData($table, $id, $newDate, $fieldId, $fieldGroupBy, $entity)
    {
        if ($table == 'order') {
            $column = 'sum(used)';
        } else if ($table == 'debts'){
            $column = 'sum(currentIndexAuto)';
        } else {
            throw new HttpException(500, 'Wrong table name');
        }
        $result = [];
        $j = 0;
        for ($i = 0; $i < count($newDate)/2; $i++) {

            $timeStart = $newDate[$j++];
            $timeEnd = $newDate[$j++];

            $result[$i]['from'] = $timeStart;
            $result[$i]['to'] = $timeEnd;

            $kv = (new Query())->select($column)
                ->from($table)
                ->where(['and', [$fieldId => $id], ['between', 'date', $timeStart, $timeEnd], ['consumer.status' => 1]])
                ->andWhere(['apartments.status' => 1])

                ->groupBy($fieldGroupBy);

            if ($table == order) {
                $kv->where[] = [$table.'.status' => 1]; // only active orders
            }

            $kv->join[] = ['INNER JOIN', 'consumer', 'consumer.idConsumer = '.$table.'.idConsumer'];
            $joins = self::buildQuery($entity);

            foreach ($joins as $oneJoin) {
                $kv->join[] = $oneJoin;
            }

            $result[$i]['data'] = $kv->all()[0][$column];
        }

        return $result;
    }

}
