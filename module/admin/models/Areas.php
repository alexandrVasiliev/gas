<?php

namespace app\module\admin\models;

use yii\db\Query;
use Yii;
use app\components\ListBehavior;
use app\components\ActiveRecord;
use app\components\CheckLink;

/**
 * This is the model class for table "areas".
 *
 * @property integer $idArea
 * @property string $area
 * @property integer $idRegion
 *
 * @property Regions $idRegion0
 * @property Cities[] $cities
 */
class Areas extends ActiveRecord
{

    public function behaviors()
    {
        return [
            'List' =>
                [
                    'class' => ListBehavior::className(),
                    'className' => Cities::className(),
                    'value1' => 'idArea',
                    'value2' => 'idCity',
                    'value3' => 'city',
            ],
            'isLink' =>
                [
                    'class' => CheckLink::className(),
                    'className' => Cities::className(),
                    'value1' => 'idArea',
            ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'areas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idRegion', 'area'], 'required'],
            [['idRegion'], 'integer'],
            [['area'], 'string', 'max' => 50],
            ['area', 'uniqueAreaInRegion', 'on' => 'create'],
        ];
    }

    public function uniqueAreaInRegion($attribute, $param)
    {
        if (in_array($this->$attribute, (new Areas())->getAreaValues($this->idRegion))) {
            $this->addError($attribute, 'Such area exists in the region');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idArea' => 'Id Area',
            'area' => 'Area',
            'idRegion' => 'Region',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['idRegion' => 'idRegion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(Cities::className(), ['idArea' => 'idArea']);
    }

    public function getAllAreas()
    {
        return $this->find()->where(['status' => 1])->all();
    }

    public static function getAreas($id, $timeStart, $timeEnd) // get areas from .. to
    {
        return  (new Query)->select('areas.idArea, area, idRegion')
            ->from('areas')
            ->innerJoin('cities', 'areas.idArea = cities.idArea')
            ->innerJoin('streets', 'cities.idCity = streets.idCity')
            ->innerJoin('houses', 'streets.idStreet = houses.idStreet')
            ->innerJoin('apartments', 'houses.idHouse = apartments.idHouse')
            ->innerJoin('consumer', 'apartments.idApartment = consumer.idApartment')
            ->innerJoin('order', 'consumer.idConsumer = order.idConsumer')
            ->where(['areas.idRegion' => $id])
            ->andWhere('date between :start and :end')
            ->andWhere(['areas.status' => 1])
            ->params([':start' => $timeStart, ':end' => $timeEnd])
            ->groupBy('areas.idArea')
            ->all();
    }

    public static function getKvByArea($id, $newDate) // get used by area
    {
        return Consumption::getData('order', $id, $newDate, 'areas.idArea', 'areas.area', Consumption::AREA);
    }

    public static function getDebtsByArea($id, $newDate) // get debt by area
    {
        return Consumption::getData('debts', $id, $newDate, 'areas.idArea', 'areas.area', Consumption::AREA);
    }

    public static function getAreaValues($id)
    {
        $query = (new Query())->select('area')->from('areas')->where(['idRegion' => $id])->all();
        $result = [];
        foreach($query as $area) {
            $result[] = $area['area'];
        }
        return $result;
    }
}
