<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\admin\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\module\admin\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idOrder', 'used'], 'integer'],
            [['date', 'idCashier'], 'safe'],
            [['tariffAfter250', 'tariffUpTo250', 'paid'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find()->where(['order.status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('user');

        $query->andFilterWhere([
            'idOrder' => $this->idOrder,
            'date' => $this->date,
            'tariffAfter250' => $this->tariffAfter250,
            'tariffUpTo250' => $this->tariffUpTo250,
            'used' => $this->used,
            'paid' => $this->paid,
            'user.username' => $this->idCashier,
        ]);

        return $dataProvider;
    }
}
