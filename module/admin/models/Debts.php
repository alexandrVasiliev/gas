<?php

namespace app\module\admin\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "debts".
 *
 * @property integer $idDebt
 * @property integer $currentIndexAuto
 * @property string $date
 * @property integer $unpaidCurrentMonth
 * @property integer $idConsumer
 *
 * @property Consumer $idConsumer0
 */
class Debts extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'debts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currentIndexAuto', 'unpaidCurrentMonth', 'idConsumer'], 'integer'],
            [['date'], 'safe'],
            ['idConsumer', 'uniqueByDate'],
        ];
    }

    public function uniqueByDate($attribute, $param)
    {
        $state = Debts::find()
            ->where(['idConsumer' => $this->$attribute])
            ->andWhere(['date' => date('Y-m-d')])->exists();
        if ($state) {
            $this->addError($attribute, 'For this client already has a record today');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idDebt' => 'Id Debt',
            'currentIndexAuto' => 'Current Index Auto',
            'date' => 'Date',
            'unpaidCurrentMonth' => 'Unpaid Current Month',
            'idConsumer' => 'Id Consumer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsumer()
    {
        return $this->hasOne(Consumer::className(), ['idConsumer' => 'idConsumer']);
    }

    public static function getLastDebt($id)
    {
        $id = Debts::find()->where(['idConsumer' => $id])->orderBy(['idDebt' => SORT_DESC])->one();
        return $id->unpaidCurrentMonth;
    }

    public function getUsedByDate($idConsumer, $date)
    {
        $used = (new Query)->select('sum(used)')
            ->from('order')
            ->where(['idConsumer' => $idConsumer])
            ->andWhere(['date' => $date])
            ->all();

        return (count($used) == 0)? $used : 0;
    }

}
