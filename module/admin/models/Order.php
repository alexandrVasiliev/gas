<?php

namespace app\module\admin\models;

use Yii;
use app\models\User;
use yii\db\Query;

/**
 * This is the model class for table "order".
 *
 * @property integer $idOrder
 * @property string $date
 * @property double $tariffAfter250
 * @property double $tariffUpTo250
 * @property integer $used
 * @property double $paid
 * @property integer $idCashier
 *
 * @property Consumer[] $consumers
 * @property Users $idCashier0
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $timeStart;
    public $timeEnd;
    public $period;

    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['used', 'idConsumer'], 'required'],
            [['timeStart', 'timeEnd', 'period'], 'required', 'on' => 'stat'],
            [['timeStart', 'timeEnd', 'period' ,'date'], 'safe'],
            [['tariffAfter250', 'tariffUpTo250', 'paid'], 'number'],
            [['used', 'idCashier', 'idConsumer'], 'integer'],
            [['used'], 'usedValue'],
            [['timeStart', 'timeEnd'], 'checkDates'],
            [['timeEnd'], 'checkTimeEnd'],
        ];
    }

    public function usedValue($attribute, $params)
    {
        if ((int)$this->{$attribute} < 1 ) {
            $this->addError($attribute, 'Value must be > 0');
        }
    }

    public function checkTimeEnd($attribute, $params)
    {
        if (strtotime($this->timeStart) > strtotime($this->timeEnd) ) {
            $this->addError($attribute, 'Time end must be bigger than time start');
        }
    }

    public function checkDates($attribute, $params)
    {
        if (strtotime($this->{$attribute}) > strtotime(date('Y-m-d'))) {
            $this->addError($attribute, 'Date must be smaller');
        }
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'idOrder' => 'Id Order',
            'date' => 'Date',
            'tariffAfter250' => 'Tariff After 250',
            'tariffUpTo250' => 'Tariff Up To 250',
            'used' => 'Used',
            'paid' => 'Paid',
            'idCashier' => 'Cashier',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashier()
    {
        return $this->hasOne(Users::className(), ['idUser' => 'idCashier']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'idCashier']);
    }

    public function getConsumer()
    {
        return $this->hasMany(Consumer::className(), ['idConsumer' => 'idConsumer']);
    }

    public function getLastIndex()
    {   
        $id = $this->find()->orderBy(['idOrder' => SORT_DESC])->one();
        return $id->idOrder;
    }

    public function calculateOrder($model)
    {
        $model->date = date('Y-m-d');
        $model->name = 'order#' . (1000 + ($model->getLastIndex() + 1));
        $model->tariffUpTo250 = (new Query)->select('tariffUpTo250')->from('tariff')->one();
        $model->tariffAfter250 = (new Query)->select('tariffAfter250')->from('tariff')->one();
        $model->tariffUpTo250 = $model->tariffUpTo250['tariffUpTo250'];
        $model->tariffAfter250 = $model->tariffAfter250['tariffAfter250'];
        $model->idCashier = Yii::$app->user->id;
        $benefit = (new Query())->select('benefit, limit')->from('benefit')
            ->innerjoin('clients', 'benefit.idBenefit = clients.idBenefit')
            ->innerjoin('consumer', 'clients.idClient = consumer.idClient')
            ->where('consumer.idConsumer = ' . $model->idConsumer)
            ->all();
        $model->status = 1;
        if ($model->used <= $benefit['limit']) {
            $model->paid = $model->used * $benefit['benefit'];
        } else {
            $model->paid = ($benefit['limit'] * $benefit['benefit']);//
            $left = ($model->used - $benefit['limit']);
            if ($model->used <= 250) {
                $model->paid += $left * $model->tariffUpTo250;
            } else {
                $model->paid += $left * $model->tariffAfter250;
            }
        }
        if (!$model->save()) {
            return $model->getErrors();
        }
    }

}
