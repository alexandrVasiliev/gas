<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\admin\models\Regions;

/**
 * RegionsSearch represents the model behind the search form about `app\module\admin\models\Regions`.
 */
class RegionsSearch extends Regions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idRegion'], 'integer'],
            [['region'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Regions::find()->where(['status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idRegion' => $this->idRegion,
        ]);

        $query->andFilterWhere(['like', 'region', $this->region]);

        return $dataProvider;
    }

    public static function searchRegions($search){
        $regions = Regions::find()
            ->select('idRegion, region')
            ->where(['like', 'region', $search])
            ->orWhere(['like', 'regions.idRegion', $search])
            ->andWhere(['status' => 1])
            ->asArray()->all();

        foreach($regions as &$region) {
            $region['type'] = 'region';
        }

        return $regions;
    }
}
