<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\admin\models\Benefit;

/**
 * BenefitSearch represents the model behind the search form about `app\module\admin\models\Benefit`.
 */
class BenefitSearch extends Benefit
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idBenefit', 'limit'], 'integer'],
            [['benefit'], 'number'],
            [['nameBenefit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Benefit::find()->where(['status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idBenefit' => $this->idBenefit,
            'benefit' => $this->benefit,
            'limit' => $this->limit,
        ]);

        $query->andFilterWhere(['like', 'nameBenefit', $this->nameBenefit]);

        return $dataProvider;
    }
}
