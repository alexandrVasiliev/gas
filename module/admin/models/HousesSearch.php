<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\admin\models\Houses;

/**
 * HousesSearch represents the model behind the search form about `app\module\admin\models\Houses`.
 */
class HousesSearch extends Houses
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idHouse'], 'integer'],
            [['house', 'idStreet'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Houses::find()->where(['houses.status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('street');

        $query->andFilterWhere([
            'idHouse' => $this->idHouse,
            'streets.street' => $this->idStreet,
        ]);

        $query->andFilterWhere(['like', 'house', $this->house]);

        return $dataProvider;
    }

    public static function searchHouses($search)
    {
        $houses = Houses::find()
            ->select('idHouse, house, street, city, area, region')
            ->innerJoin('streets', 'houses.idStreet = streets.idStreet')
            ->innerJoin('cities', 'cities.idCity = streets.idCity')
            ->innerJoin('areas', 'cities.idArea = areas.idArea')
            ->innerJoin('regions', 'regions.idRegion = areas.idRegion')
            ->where(['like', 'house', $search])
            ->orWhere(['like', 'houses.idHouse', $search])
            ->andWhere(['houses.status' => 1])
            ->asArray()->all();

        foreach($houses as &$house) {
            $house['type'] = 'house';
        }

        return $houses;
    }
}
