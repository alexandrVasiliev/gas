<?php

namespace app\module\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\module\admin\models\Areas;

/**
 * AreasSearch represents the model behind the search form about `app\module\admin\models\Areas`.
 */
class AreasSearch extends Areas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idArea', 'idRegion'], 'integer'],
            [['area'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Areas::find()->where(['status' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'idArea' => $this->idArea,
            'idRegion' => $this->idRegion,
        ]);

        $query->andFilterWhere(['like', 'area', $this->area]);

        return $dataProvider;
    }

    public static function searchAreas($search)
    {
        $areas = Areas::find()
            ->select('idArea, area, region')
            ->innerJoin('regions', 'regions.idRegion = areas.idRegion')
            ->where(['like', 'area', $search])
            ->orWhere(['like', 'areas.idArea', $search])
            ->andWhere(['areas.status' => 1])
            ->asArray()->all();

        foreach($areas as &$area) {
            $area['type'] = 'area';
        }

        return $areas;
    }
}
