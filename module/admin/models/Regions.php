<?php

namespace app\module\admin\models;

use Yii;
use app\components\ListBehavior;
use app\components\ActiveRecord;
use app\components\CheckLink;

/**
 * This is the model class for table "regions".
 *
 * @property integer $idRegion
 * @property string $region
 *
 * @property Areas[] $areas
 */

class Regions extends ActiveRecord
{

    public function behaviors()
    {
        return [
            'List' =>
                [
                    'class' => ListBehavior::className(),
                    'className' => Areas::className(),
                    'value1' => 'idRegion',
                    'value2' => 'idArea',
                    'value3' => 'area',
                ],
            'isLink' =>
                [
                    'class' => CheckLink::className(),
                    'className' => Areas::className(),
                    'value1' => 'idRegion',
                ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['region', 'idProvider'], 'required'],
            [['region'], 'string', 'max' => 255],
            ['region', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idRegion' => 'Id Region',
            'region' => 'Region',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreas()
    {
        return $this->hasMany(Areas::className(), ['idRegion' => 'idRegion']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Providers::className(), ['idProvider' => 'idProvider']);
    }

    public function getAllRegions() // get active regions
    {
        return $this->find()->where(['status' => 1])->all();
    }

    public static function getRegions($timeStart, $timeEnd) // get regions from .. to
    {
        return Regions::find()
            ->innerJoin('areas', 'regions.idRegion = areas.idRegion')
            ->innerJoin('cities', 'areas.idArea = cities.idArea')
            ->innerJoin('streets', 'cities.idCity = streets.idCity')
            ->innerJoin('houses', 'streets.idStreet = houses.idStreet')
            ->innerJoin('apartments', 'houses.idHouse = apartments.idHouse')
            ->innerJoin('consumer', 'apartments.idApartment = consumer.idApartment')
            ->innerJoin('order', 'consumer.idConsumer = order.idConsumer')
            ->where('date between :start and :end')
            ->andWhere(['regions.status' => 1])
            ->andWhere(['apartments.status' => 1])
            ->params([':start' => $timeStart, ':end' => $timeEnd])
            ->groupBy('regions.idRegion')
            ->all();
    }

    public static function getKvByRegion($id, $newDate) // get used by region
    {
        return (new Consumption)->getData('order', $id, $newDate, 'regions.idRegion', 'regions.region', Consumption::REGION);
    }

    public static function getDebtsByRegion($id, $newDate) // get debt by region
    {
        return (new Consumption)->getData('debts', $id, $newDate, 'regions.idRegion', 'regions.region', Consumption::REGION);
    }

}
