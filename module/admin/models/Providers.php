<?php

namespace app\module\admin\models;

use Yii;
use app\components\ActiveRecord;
use app\components\CheckLink;

/**
 * This is the model class for table "providers".
 *
 * @property integer $idProvider
 * @property integer $kc
 * @property string $pr
 * @property string $mfo
 * @property string $edrpoy
 *
 * @property Consumer[] $consumers
 */
class Providers extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'isLink' =>
                [
                    'class' => CheckLink::className(),
                    'className' => Regions::className(),
                    'value1' => 'idProvider',
                ]
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'providers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kc', 'pr', 'mfo', 'edrpoy', 'name'], 'required'],
            [['kc'], 'integer'],
            [['pr', 'edrpoy'], 'string', 'max' => 11],
            [['mfo'], 'string', 'max' => 6],
            [['pr'], 'string', 'min' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProvider' => 'Id Provider',
            'kc' => 'Kc',
            'pr' => 'Pr',
            'mfo' => 'Mfo',
            'edrpoy' => 'Edrpoy',
            'name' => 'Provider name'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getAllProviders() // get active providers
    {
        return $this->find()->where(['status' => 1])->all();
    }

    public function getConsumers()
    {
        return $this->hasMany(Consumer::className(), ['idProvider' => 'idProvider']);
    }
}
