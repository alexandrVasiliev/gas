<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Houses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="houses-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idRegion')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($regions, 'idRegion', 'region'),
        'language' => 'en',
        'options' => [
            'onchange' => '
                        $.post( "../../admin/regions/list?id='.'"+$(this).val(), function( data ) {
                            $( "select#houses-idarea").html( data);
                        });',
            'placeholder' => 'Select a region ...',
        ],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);?>

    <?= $form->field($model, 'idArea')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($areas, 'idArea', 'area'),
        'language' => 'en',
        'options' => [
            'onchange' => '
                        $.post( "../../admin/areas/list?id='.'"+$(this).val(), function( data ) {
                            $( "select#houses-idcity").html( data);
                        });',
            'placeholder' => 'Select a area ...',
        ],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);?>

    <?= $form->field($model, 'idCity')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($cities, 'idCity', 'city'),
        'language' => 'en',
        'options' => [
            'onchange' => '
                        $.post( "../../admin/cities/list?id='.'"+$(this).val(), function( data ) {
                            $( "select#houses-idstreet").html( data);
                        });',
            'placeholder' => 'Select a city ...',
        ],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);?>

    <?= $form->field($model, 'idStreet')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($streets, 'idStreet', 'street'),
        'language' => 'en',
        'options' => [
            'placeholder' => 'Select a street ...',
        ],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);?>

    <?= $form->field($model, 'house')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
