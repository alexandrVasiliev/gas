<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Houses */

$this->title = 'Update Houses: ' . ' ' . $model->idHouse;
$this->params['breadcrumbs'][] = ['label' => 'Houses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idHouse, 'url' => ['view', 'id' => $model->idHouse]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="houses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cities' => $cities,
        'regions' => $regions,
        'areas' => $areas,
        'streets' => $streets,
    ]) ?>

</div>
