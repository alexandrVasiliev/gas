<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(['method' => 'POST']); ?>

    <?=$form->field($model, 'firstname')->input('text', ['value' => $clientDetails[0]])?>

    <?=$form->field($model, 'middlename')->input('text', ['value' => $clientDetails[1]])?>

    <?=$form->field($model, 'lastname')->input('text', ['value' => $clientDetails[2]])?>


    <?= $form->field($model, 'idBenefit')->widget(Select2::classname(), [
            'data' => ArrayHelper::map($benefits, 'idBenefit', 'nameBenefit'),
            'language' => 'en',
            'options' => ['placeholder' => 'Select a benefit ...'],
            'pluginOptions' => [
            'allowClear' => true
            ],
        ]);?>


        <?= $form->field($model, 'personalAccount')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
