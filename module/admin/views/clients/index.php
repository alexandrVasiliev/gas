<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\module\admin\models\ClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="clients-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('Create Clients', ['value' => Url::to('/admin/clients/create'),'class' => 'btn btn-success', 'id' => 'modalButton']) ?>
    </p>

    <?php
        Modal::begin([
            'header' => '<h4>Create clients</h4>',
            'id' => 'modal',
            'size' => 'modal-lg',
        ]);

        echo "<div id='modalContent'></div>";

        Modal::end();
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'attribute' => 'idPerson',
                'value' => 'person.firstname',
            ],
            [
                'attribute' => 'idPerson',
                'value' => 'person.middlename',
            ],
            [
                'attribute' => 'idPerson',
                'value' => 'person.lastname',
            ],
            [
                'attribute' => 'idBenefit',
                'value' => 'benefit.nameBenefit',
            ],

            'personalAccount',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'.$opportunities
            ],
        ],
    ]); ?>

</div>
