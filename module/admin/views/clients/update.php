<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Clients */

$this->title = 'Update Clients: ' . ' ' . $model->idClient;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idClient, 'url' => ['view', 'id' => $model->idClient]];
$this->params['breadcrumbs'][] = 'Update';

?>

<div class="clients-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'persons' => $persons,
        'benefits' => $benefits,
        'clientDetails' => $clientDetails,
    ]) ?>

</div>
