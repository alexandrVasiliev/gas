<?php
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * Date: 12.05.15
 * Time: 16:31
 * @var $search string
 */
?>
<br><br>
<?php if (!empty($similarWords)) {?>

    <div class="alert alert-warning">Did you mean <?=Html::a($similarWords, Url::to('/admin/'.$similarWords))?> ?</div>

<?php } else {?>

    <?php if (count($results) == 0) { ?>

        <div class="alert alert-danger">The result of the query: <strong><?=$search?></strong> given nothing</div>

    <?php } else {?>

        <div class="alert alert-success">The query results: <?=$search?> found <?=$pagination->totalCount?> results (<?=$time?> seconds)</div>

        <?= LinkPager::widget([
            'pagination' => $pagination,
        ]);?>

        <div class="list-group">
            <ul class="list-group">
                <?php foreach($results as $result) :?>
                    <?php
                        if ($result['type'] == 'person') {
                            print '<li class="list-group-item list-group-item-warning">'.Html::a($result['firstname']." ".$result['middlename']." ".$result['lastname'], Url::to(['/admin/persons/view', 'id' => $result['idPerson']])).'</li>';
                        } else if ($result['type'] == 'region') {
                            print '<li class="list-group-item list-group-item-warning">'.Html::a($result['region'], Url::to(['/admin/regions/view', 'id' => $result['idRegion']])).'</li>';
                        } else if ($result['type'] == 'area') {
                            print '<li class="list-group-item list-group-item-warning">'.Html::a($result['area']." ".$result['region'], Url::to(['/admin/areas/view', 'id' => $result['idArea']])).'</li>';
                        } else if ($result['type'] == 'city') {
                            print '<li class="list-group-item list-group-item-warning">'.Html::a($result['city']." ".$result['area']." ".$result['region'], Url::to(['/admin/cities/view', 'id' => $result['idCity']])).'</li>';
                        } else if ($result['type'] == 'street') {
                            print '<li class="list-group-item list-group-item-warning">'.Html::a($result['street']." ".$result['city']." ".$result['area']." ".$result['region'], Url::to(['/admin/streets/view', 'id' => $result['idStreet']])).'</li>';
                        } else if ($result['type'] == 'house') {
                            print '<li class="list-group-item list-group-item-warning">'.Html::a($result['house']." ".$result['street']." ".$result['city']." ".$result['area']." ".$result['region'], Url::to(['/admin/houses/view', 'id' => $result['idHouse']])).'</li>';
                        } else if ($result['type'] == 'apartment') {
                            print '<li class="list-group-item list-group-item-warning">'.Html::a($result['apartment']." ".$result['house']." ".$result['street']." ".$result['city']." ".$result['area']." ".$result['region'], Url::to(['/admin/apartments/view', 'id' => $result['idApartment']])).'</li>';
                        } else if ($result['type'] == 'provider') {
                            print '<li class="list-group-item list-group-item-warning">'.Html::a($result['name'], Url::to(['/admin/providers/view', 'id' => $result['idProvider']])).'</li>';
                        } else if ($result['type'] == 'consumer') {
                            print '<li class="list-group-item list-group-item-warning">'.Html::a($result['name'], Url::to(['/admin/consumer/view', 'id' => $result['idConsumer']])).'</li>';
                        }
                    ?>
                <?php endforeach;?>
            </ul>
        </div>

    <?php } }?>

