<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Cities */

$this->title = 'Update Cities: ' . ' ' . $model->idCity;
$this->params['breadcrumbs'][] = ['label' => 'Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idCity, 'url' => ['view', 'id' => $model->idCity]];
$this->params['breadcrumbs'][] = 'Update';

?>

<div class="cities-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'regions' => $regions,
        'areas' => $areas,
    ]) ?>

</div>
