<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Cities */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="cities-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idRegion')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($regions, 'idRegion', 'region'),
        'language' => 'en',
        'options' => [
            'onchange' => '
                        $.post( "../../admin/regions/list?id='.'"+$(this).val(), function( data ) {
                            $( "select#cities-idarea").html( data);
                        });',
            'placeholder' => 'Select a region ...',
        ],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);?>
    <?= $form->field($model, 'idArea')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($areas, 'idArea', 'area'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a area ...'],
        'pluginOptions' => [
        'allowClear' => true,
        ],
    ]);?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
