<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Benefit */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="benefit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'benefit')->textInput() ?>

    <?= $form->field($model, 'nameBenefit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'limit')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
