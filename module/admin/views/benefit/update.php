<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Benefit */

$this->title = 'Update Benefit: ' . ' ' . $model->idBenefit;
$this->params['breadcrumbs'][] = ['label' => 'Benefits', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idBenefit, 'url' => ['view', 'id' => $model->idBenefit]];
$this->params['breadcrumbs'][] = 'Update';

?>
<div class="benefit-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
