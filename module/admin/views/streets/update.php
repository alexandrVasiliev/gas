<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Streets */

$this->title = 'Update Streets: ' . ' ' . $model->idStreet;
$this->params['breadcrumbs'][] = ['label' => 'Streets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idStreet, 'url' => ['view', 'id' => $model->idStreet]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="streets-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'cities' => $cities,
        'areas' => $areas,
        'regions' => $regions,
    ]) ?>

</div>
