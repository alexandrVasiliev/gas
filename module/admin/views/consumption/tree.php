<?php

use \yii\bootstrap\ActiveForm;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this yii\web\View */
    $this->title = 'Consumers';
    $this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin([
    'method' => 'POST',
    'enableAjaxValidation' => true,
    'id' => 'form1'
]); ?>



        <?= $form->field($model, 'timeStart')->widget(
            DatePicker::className(), [
            'inline' => false,
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);?>

        <?= $form->field($model, 'timeEnd')->widget(
            DatePicker::className(), [
            'inline' => false,
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);?>

        <?= $form->field($model, 'period')->widget(Select2::classname(), [
            'data' => $periods,
            'language' => 'en',
            'options' => ['placeholder' => 'Select a period ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>

        <?=Html::submitButton('Download Excel file', [
            'class' => 'btn btn-primary',
            'id' => 'statistic'
        ])?>

    <div class='background'>
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <?php ActiveForm::end()?>
