<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Apartments */

$this->title = 'Update Apartments: ' . ' ' . $model->idApartment;
$this->params['breadcrumbs'][] = ['label' => 'Apartments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idApartment, 'url' => ['view', 'id' => $model->idApartment]];
$this->params['breadcrumbs'][] = 'Update';

?>
<div class="apartments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'regions' => $regions,
        'areas' => $areas,
        'cities' => $cities,
        'streets' => $streets,
        'houses' => $houses,
    ]) ?>

</div>
