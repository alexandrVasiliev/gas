<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'used')->textInput() ?>

    <?= $form->field($model, 'idConsumer')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($consumers, 'idConsumer', 'name'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a consumer ...'],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
