<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idOrder') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'tariffAfter250') ?>

    <?= $form->field($model, 'tariffUpTo250') ?>

    <?= $form->field($model, 'used') ?>

    <?php // echo $form->field($model, 'paid') ?>

    <?php // echo $form->field($model, 'idCashier') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
