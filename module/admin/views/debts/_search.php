<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\DebtsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="debts-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idDebt') ?>

    <?= $form->field($model, 'currentIndexAuto') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'unpaidCurrentMonth') ?>

    <?= $form->field($model, 'idConsumer') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
