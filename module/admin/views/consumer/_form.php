<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Consumer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="consumer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idTariff')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($tariff, 'idTariff', 'name'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a client tariff ...'],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);?>

    <?= $form->field($model, 'idStreet')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($streets, 'idStreet', 'street'),
        'language' => 'en',
        'options' => [
            'onchange' => '
                        $.post( "../../admin/houses/list?id2='.'"+$(this).val(), function( data ) {
                            $( "select#consumer-idhouse").html( data);
                        });',
            'placeholder' => 'Select a street ...',
        ],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);?>

    <?= $form->field($model, 'idHouse')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($houses, 'idHouse', 'house'),
        'language' => 'en',
        'options' => [
            'onchange' => '
                        $.post( "../../admin/apartments/list?id='.'"+$(this).val(), function( data ) {
                            $( "select#consumer-idapartment").html( data);
                        });',
            'placeholder' => 'Select a house ...',
        ],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);?>

    <?= $form->field($model, 'idApartment')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($apartments, 'idApartment', 'apartment'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a apartment ...'],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);?>

    <?= $form->field($model, 'idClient')->widget(Select2::classname(), [
        'data' => ArrayHelper::map($clients, 'idClient', 'personalAccount'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a client account ...'],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ]);?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>