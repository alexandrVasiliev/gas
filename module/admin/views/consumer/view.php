<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\module\admin\models\Consumer */

$this->title = $model->idConsumer;
$this->params['breadcrumbs'][] = ['label' => 'Consumers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consumer-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('admin')) : ?>

        <p>
            <?= Html::a('Update', ['update', 'id' => $model->idConsumer], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->idConsumer], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idConsumer',
            'name',
            'tariff.name',
            'apartment.apartment',
            'apartment.house.house',
            'apartment.house.street.street',
            'apartment.house.street.city.city',
            'apartment.house.street.city.area.area',
            'apartment.house.street.city.area.region.region',
            'client.person.firstname',
            'client.person.middlename',
            'client.person.lastname',
        ],
    ]) ?>

</div>
