<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\module\admin\models\ConsumerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consumers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consumer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('admin')): ?>
        <p>
            <?= Html::button('Create Consumer', ['value' => Url::to('/admin/consumer/create'), 'class' => 'btn btn-success', 'id' => 'modalButton']) ?>
        </p>

        <?php
            Modal::begin([
                'header' => '<h4>Create consumer</h4>',
                'id' => 'modal',
                'size' => 'modal-lg',
            ]);

            echo "<div id='modalContent'></div>";

            Modal::end();
        ?>

    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'tariff.name',
            'apartment.house.street.city.area.region.provider.name',
            'apartment.house.street.city.area.region.region',
            'apartment.house.street.city.area.area',
            'apartment.house.street.city.city',
            'apartment.house.street.street',
            'apartment.house.house',
            'apartment.apartment',
            'client.person.firstname',
            'client.person.lastname',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'.$opportunities
            ],
        ],
    ]); ?>

</div>
