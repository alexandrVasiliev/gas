<?php

namespace app\module\admin\controllers;

use app\module\admin\models\Clients;
use app\module\admin\models\Houses;
use app\module\admin\models\Apartments;
use app\module\admin\models\Streets;
use app\module\admin\models\Tariff;
use Yii;
use app\module\admin\models\Consumer;
use app\module\admin\models\ConsumerSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
/**
 * ConsumerController implements the CRUD actions for Consumer model.
 */
class ConsumerController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Consumer models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('cashier')) {

            $searchModel = new ConsumerSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            if (Yii::$app->user->can('admin')) {
                $opportunities = '{update}{delete}';
            }
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'opportunities' => $opportunities,
            ]);
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Displays a single Consumer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('cashier')) {

            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Creates a new Consumer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('admin')) {

            $model = new Consumer();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->name = "#" . (1000 + ($model->getLastIndex() + 1));
                $model->status = 1;
                if (!$model->save()) {
                    return $model->getErrors();
                }
                return $this->redirect(['view', 'id' => $model->idConsumer]);
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'tariff' => (new Tariff)->getAllTariff(),
                    'streets' => (new Streets)->getAllStreets(),
                    'houses' => (new Houses)->getAllHouses(),
                    'apartments' => (new Apartments)->getAllApartments(),
                    'clients' => (new Clients)->getAllClients(),
                ]);
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }
    /**
     * Updates an existing Consumer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can('admin')) {

            if ((new Consumer)->isLink($id)) {

                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->idConsumer]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                        'tariff' => (new Tariff)->getAllTariff(),
                        'streets' => (new Streets)->getAllStreets(),
                        'houses' => (new Houses)->getAllHouses(),
                        'apartments' => (new Apartments)->getAllApartments(),
                        'clients' => (new Clients)->getAllClients(),
                    ]);
                }
            } else {
                Consumer::alertError('Not empty consumer.');
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Deletes an existing Consumer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('admin')) {

            if ((new Consumer)->isLink($id)) {

                $model = $this->findModel($id);
                $model->status = 0;

                if (!$model->save()) {
                    return $model->getErrors();
                }
                return $this->redirect(['index']);
            } else {
                Consumer::alertError('Not empty consumer.');
            }

        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Finds the Consumer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Consumer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Consumer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
