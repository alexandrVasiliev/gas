<?php

namespace app\module\admin\controllers;

use app\module\admin\models\Consumer;
use Yii;
use app\module\admin\models\Debts;
use app\module\admin\models\DebtsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * DebtsController implements the CRUD actions for Debts model.
 */

class DebtsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Debts models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('admin')) {

            $searchModel = new DebtsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Displays a single Debts model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('admin')) {

            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Creates a new Debts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('admin')) {

            $model = new Debts();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->date = date('Y-m-d');
                $paid = (new Debts())->getUsedByDate($model->idConsumer, $model->date);
                $model->unpaidCurrentMonth = (int)Debts::getLastDebt($model->idConsumer) + ( $model->currentIndexAuto - $paid[0]['sum(used)']);
                if (!$model->save()) {
                    return $model->getErrors();
                }
                return $this->redirect(['view', 'id' => $model->idDebt]);
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'consumers' => (new Consumer)->getAllConsumers(),
                ]);
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }


    /**
     * Finds the Debts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Debts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Debts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
