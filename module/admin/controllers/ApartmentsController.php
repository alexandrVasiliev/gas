<?php

namespace app\module\admin\controllers;

use app\module\admin\models\Areas;
use app\module\admin\models\Cities;
use app\module\admin\models\Regions;
use Yii;
use app\module\admin\models\Apartments;
use app\module\admin\models\Streets;
use app\module\admin\models\Houses;
use app\module\admin\models\ApartmentsSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * ApartmentsController implements the CRUD actions for Apartments model.
 */
class ApartmentsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Apartments models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('admin')) {

            $searchModel = new ApartmentsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Displays a single Apartments model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('admin')) {

            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }


    /**
     * Creates a new Apartments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('admin')) {
            $model = new Apartments(['scenario' => 'create']);

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $model->status = 1;

                if (!$model->save()){
                    return $model->getErrors();
                }

                return $this->redirect(['view', 'id' => $model->idApartment]);
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'regions' => (new Regions)->getAllRegions(),
                    'areas' => (new Areas)->getAllAreas(),
                    'cities' => (new Cities)->getAllCities(),
                    'streets' => (new Streets)->getAllStreets(),
                    'houses' => (new Houses)->getAllHouses(),
                ]);
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Updates an existing Apartments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can('admin')) {

            if ((new Apartments)->isLink($id)) {

                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->idApartment]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                        'regions' => (new Regions)->getAllRegions(),
                        'areas' => (new Areas)->getAllAreas(),
                        'cities' => (new Cities)->getAllCities(),
                        'streets' => (new Streets)->getAllStreets(),
                        'houses' => (new Houses)->getAllHouses(),
                    ]);
                }
            } else {
                Apartments::alertError('Not empty apartment.');
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Deletes an existing Apartments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('admin')) {

            if ((new Apartments)->isLink($id)) {

                $model = $this->findModel($id);

                $model->status = 0;

                if (!$model->save()) {
                    return $model->getErrors();
                }
                return $this->redirect(['index']);
            } else {
                Apartments::alertError('Not empty apartment.');
            }

        } else {
            throw new ForbiddenHttpException('Access forbidden');

        }
    }

    /**
     * Finds the Apartments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apartments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Apartments::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
