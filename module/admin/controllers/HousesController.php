<?php

namespace app\module\admin\controllers;

use app\module\admin\models\Areas;
use app\module\admin\models\Cities;
use app\module\admin\models\Regions;
use app\module\admin\models\Streets;
use Yii;
use app\module\admin\models\Houses;
use app\module\admin\models\HousesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

/**
 * HousesController implements the CRUD actions for Houses model.
 */

class HousesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Houses models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('admin')) {

            $searchModel = new HousesSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Displays a single Houses model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('admin')) {

            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Creates a new Houses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('admin')) {

            $model = new Houses(['scenario' => 'create']);

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $model->status = 1;

                if (!$model->save()){
                    return $model->getErrors();
                }

                return $this->redirect(['view', 'id' => $model->idHouse]);
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'regions' => (new Regions)->getAllRegions(),
                    'areas' => (new Areas)->getAllAreas(),
                    'cities' => (new Cities)->getAllCities(),
                    'streets' => (new Streets)->getAllStreets(),
                ]);
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Updates an existing Houses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can('admin')) {

            if ((new Houses)->isLink($id)) {

                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->idHouse]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                        'regions' => (new Regions)->getAllRegions(),
                        'areas' => (new Areas)->getAllAreas(),
                        'cities' => (new Cities)->getAllCities(),
                        'streets' => (new Streets)->getAllStreets(),
                    ]);
                }
            } else {
                Houses::alertError('Not empty house.');
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Deletes an existing Houses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('admin')) {

            if ((new Houses)->isLink($id)) {

                $model = $this->findModel($id);
                $model->status = 0;

                if (!$model->save()) {
                    return $model->getErrors();
                }
                return $this->redirect(['index']);
            } else {
                Houses::alertError('Not empty house.');
            }

        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Finds the Houses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Houses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Houses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionList($id)
    {
        return (new Houses)->getOptionsList($id);
    }
}
