<?php

namespace app\module\admin\controllers;

use app\module\admin\models\Regions;
use Yii;
use app\module\admin\models\Areas;
use app\module\admin\models\AreasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

/**
 * AreasController implements the CRUD actions for Areas model.
 */

class AreasController extends Controller
{
    public function behaviors()
    {
        return [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Areas models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('admin')) {
            $searchModel = new AreasSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Displays a single Areas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('admin')) {

            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);

        }else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Creates a new Areas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('admin')) {

            $model = new Areas(['scenario' => 'create']);

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $model->status = 1;

                if (!$model->save()) {
                    return $model->getErrors();
                }

                return $this->redirect(['view', 'id' => $model->idArea]);
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'regions' => (new Regions)->getAllRegions(),
                ]);
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Updates an existing Areas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->can('admin')) {

            if ((new Areas)->isLink($id)) {


                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->idArea]);
                } else {
                    return $this->render('update', [
                        'model' => $model,
                        'regions' => (new Regions)->getAllRegions(),
                    ]);
                }
            } else {
                Areas::alertError('Not empty area.');
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Deletes an existing Areas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('admin')) {

            if ((new Areas)->isLink($id)) {

                $model = $this->findModel($id);
                $model->status = 0;

                if (!$model->save()) {
                    return $model->getErrors();
                }
                return $this->redirect(['index']);
            } else {
                Areas::alertError('Not empty area.');
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Finds the Areas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Areas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Areas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionList($id)
    {
        return (new Areas)->getOptionsList($id);
    }
}
