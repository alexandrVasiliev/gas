<?php

namespace app\module\admin\controllers;

use app\module\admin\models\Benefit;
use app\module\admin\models\Persons;
use Yii;
use app\module\admin\models\Clients;
use app\module\admin\models\ClientsSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

/**
 * ClientsController implements the CRUD actions for Clients model.
 */

class ClientsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Clients models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('cashier')) {

            $searchModel = new ClientsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            if (Yii::$app->user->can('admin')) {
                $opportunities = '{update}{delete}';
            }
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'opportunities' => $opportunities,
            ]);
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('cashier')) {

            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }else {
                throw new ForbiddenHttpException('Access forbidden');
            }
    }

    /**
     * Creates a new Clients model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('cashier')) {

            $model = new Clients();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $personModel = new Persons(['scenario' => 'create']);

                $personModel->firstname = $model->firstname;
                $personModel->middlename = $model->middlename;
                $personModel->lastname = $model->lastname;

                if ($personModel->validate()) {

                    $personModel->save();
                    $model->idPerson = $personModel->idPerson;
                    $model->status = 1;
                    if (!$model->save()) {
                        return $model->getErrors();
                    };

                    return $this->redirect(['view', 'id' => $model->idClient]);
                } else {
                    Clients::alertError('Person already exists');
                }
            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'benefits' => (new Benefit())->getAllBenefits(),
                ]);
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Updates an existing Clients model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $personModel = Persons::findOne($id);

        if (Yii::$app->user->can('admin')) {

            if ((new Clients)->isLink($id)) {

                $model = $this->findModel($id);

                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    $personModel = new Persons();

                    $personModel->firstname = $model->firstname;
                    $personModel->middlename = $model->middlename;
                    $personModel->lastname = $model->lastname;

                    if ($personModel->validate()) {

                        $personModel->save();
                        $model->idPerson = $personModel->idPerson;
                        $model->save();
                        return $this->redirect(['view', 'id' => $model->idClient]);

                    } else {
                        Clients::alertError('Person already exists');
                    }
                } else {
                    return $this->render('update', [
                        'model' => $model,
                        'persons' => (new Persons)->getAllPersons(),
                        'benefits' => (new Benefit)->getAllBenefits(),
                        'clientDetails' => [$personModel->firstname, $personModel->middlename, $personModel->lastname],
                    ]);
                }
            } else {
                Clients::alertError('Not empty clients.');
            }

        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Deletes an existing Clients model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->can('admin')) {

            if ((new Clients)->isLink($id)) {

                $model = $this->findModel($id);
                $model->status = 0;


                if (!$model->save()) {
                    return $model->getErrors();
                }
                return $this->redirect(['index']);
            } else {
                Clients::alertError('Not empty clients.');
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Finds the Clients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clients::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
