<?php

namespace app\module\admin\controllers;

use app\module\admin\models\Consumer;
use Yii;
use app\module\admin\models\Order;
use app\module\admin\models\OrderSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\module\admin\models\User;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;

class OrderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->can('cashier')) {

            $searchModel = new OrderSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);

        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->user->can('cashier')) {

            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->can('cashier')) {
            $model = new Order();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                (new Order)->calculateOrder($model);

                return $this->redirect(['view', 'id' => $model->idOrder]);

            } else {
                return $this->renderAjax('create', [
                    'model' => $model,
                    'consumers' => (new Consumer())->getAllConsumers(),
                    'cashiers' => (new User())->getCashiers(),
                ]);
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->user->can('cashier') && (Yii::$app->user->id == $model->idCashier)) { // cashier can update only own orders

            if ($model->load(Yii::$app->request->post())) {

                (new Order)->calculateOrder($model);

                return $this->redirect(['view', 'id' => $model->idOrder]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'consumers' => (new Consumer)->getAllConsumers(),
                    'cashiers' => (new User)->getCashiers(),
                ]);
            }
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->user->can('cashier') && (Yii::$app->user->id == $model->idCashier) && $model->date == date('Y-m-d')) {// cashier can delete only own and new orders

            $model = $this->findModel($id);
            $model->status = 0;

            if (!$model->save()) {
                return $model->getErrors();
            }

            return $this->redirect(['index']);
        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
