<?php

namespace app\module\admin\controllers;

use app\helpers\SearchHelper;
use app\module\admin\models\ConsumerSearch;
use app\module\admin\models\ApartmentsSearch;
use app\module\admin\models\AreasSearch;
use app\module\admin\models\CitiesSearch;
use app\module\admin\models\HousesSearch;
use app\module\admin\models\PersonsSearch;
use app\module\admin\models\ProvidersSearch;
use app\module\admin\models\RegionsSearch;
use app\module\admin\models\StreetsSearch;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use app\module\admin\models\Search;
//use app\components\SphinxClient;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

//    public function actionSearch()
//    {
//        $sphinx = new SphinxClient();
//        $sphinx->SetServer(\Yii::$app->params['sphinx']['host'], \Yii::$app->params['sphinx']['port']);
//        $sphinx->SetArrayResult(true);
//        $sphinx->SetMatchMode(SPH_MATCH_ALL);
//        $sphinx->SetRankingMode(SPH_RANK_PROXIMITY);
//        $search = Yii::$app->request->get('search');
//        $result = $sphinx->Query($search);
//        if(!$result) {
//            echo "Query failed: " . $sphinx->GetLastError() . ".\n";
//        } else {
//            if ( $sphinx->GetLastWarning() ) {
//                unset($error);
//                $error = "WARNING: " . $sphinx->GetLastWarning() . " ";
//                echo $error;
//            }
//
//            if ( ! empty($result["matches"]) ) {
//                var_dump($result["matches"]);die;
//                $ids = array_keys($result["matches"]);
//                $rows = $sphinx->getLeadRecords($ids);
//            }
//        }
//    }

    public function actionSearch()
    {
        $start = microtime();
        $search = Yii::$app->request->get('search');
        $tables = ['user', 'providers', 'clients', 'debts', 'apartments', 'houses', 'streets', 'cities', 'areas', 'regions', 'benefit', 'tariff', 'persons', 'order', 'consumer'];

        //if (preg_match("/[а-яА-ЯёЁ]+$/", $search)) {
        $search = SearchHelper::translit($search);
        //}

        if (in_array($search, $tables)) {
            return $this->redirect('/admin/'.$search.'/index');
        } else {
            if (!SearchHelper::correct($search, $tables) === false) {
                return $this->render('search', [
                    'search' => $search,
                    'similarWords' => SearchHelper::correct($search, $tables), // return the most similar word
                ]);

            }
        }

        $results = PersonsSearch::searchPersons($search);

        $results = array_merge($results, RegionsSearch::searchRegions($search));

        $results = array_merge($results, AreasSearch::searchAreas($search));

        $results = array_merge($results, CitiesSearch::searchCities($search));

        $results = array_merge($results, StreetsSearch::searchStreets($search));

        $results = array_merge($results, HousesSearch::searchHouses($search));

        $results = array_merge($results, ApartmentsSearch::searchApartments($search));

        $results = array_merge($results, ProvidersSearch::searchProviders($search));

        $results = array_merge($results, ConsumerSearch::searchConsumers($search));

        $time = round(microtime() - $start, 3);

        $pagination = new Pagination([
            'totalCount' => count($results),
        ]);

        $results = array_slice($results, $pagination->offset, $pagination->limit);

        return $this->render('search', [
            'search' => $search,
            'results' => $results,
            'pagination' => $pagination,
            'time' => $time,
        ]);
    }
}
