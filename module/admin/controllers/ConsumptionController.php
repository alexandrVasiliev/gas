<?php

namespace app\module\admin\controllers;

use app\module\admin\models\Order;
use Yii;
use app\module\admin\models\Consumption;
use yii\web\ForbiddenHttpException;
use yii\widgets\ActiveForm;
use yii\web\Controller;

class ConsumptionController extends Controller
{

    public $periods = ['1' => 'Statistics by day', '2' => 'Statistics by month', '3' => 'Statistics by 3 month', '4' => 'Statistics by year', '5' => 'Statistic by month 2', '6' => 'Statistic by year 2'];

    public function actionTree()
    {
        $model = new Order(['scenario' => 'stat']);
        if (Yii::$app->user->can('admin')) {

            if (Yii::$app->request->isAjax && $model->load($_POST)) {
                Yii::$app->response->format = 'json';
                return ActiveForm::validate($model);
            }

            if ($model->load(Yii::$app->request->get())) {
                if (in_array('', [$model->period, $model->timeEnd, $model->timeStart])) {
                    Consumption::alertError('You must fill all fields');
                    exit;
                }
//                Consumption::downloadExcel($model->period, $model->timeStart, $model->timeEnd);
            } // end if

            return $this->render('tree', [
                'model' => $model,
                'periods' => $this->periods,
            ]);

        } else {
            throw new ForbiddenHttpException('Access forbidden');
        }
    }

    public function actionDownload()
    {
        $period = Yii::$app->request->post('period');
        $timeStart = Yii::$app->request->post('timestart');
        $timeEnd = Yii::$app->request->post('timeend');
        Consumption::downloadExcel($period, $timeStart, $timeEnd);
    }

    public function actionDown()
    {
        $filename = Yii::getAlias('@runtime/');
        $file = json_decode(Yii::$app->request->get('file'));
        $filename .= $file->filename;
        Yii::$app->response->sendFile($filename);
        unlink($filename);
        header('/admin/consumption/tree');
    }

}
