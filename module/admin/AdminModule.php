<?php

namespace app\module\admin;

use yii\base\Module;
use yii\filters\AccessControl;

class AdminModule extends Module
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public $controllerNamespace = 'app\module\admin\controllers';
    public $layout = 'admin';

    public function init()
    {
        parent::init();
    }
}
