$(document).ready(function() {
    $('div.spinner').css('display', 'none');

    $('#statistic').click(function(){

        var timeend = $('input#order-timeend').val(),
            timestart = $('input#order-timestart').val(),
            period = $('select#order-period').val(),
            data = {
                period: period,
                timestart: timestart,
                timeend: timeend
            }

        if(timeend && timestart && period) {
            $('div.spinner').css('display', 'block');

            $.ajax({
                type: 'POST',
                url: '/admin/consumption/download',
                data: data,
                success: function(result){
                    window.location = "/admin/consumption/down?file="+result;
                    $('div.spinner').css('display', 'none');
                },
                error:  function (result) {
                    alert('error');
                    $('div.spinner').css('display', 'none');
                }
            });
        }

    });
});